window.dimestoreCallback(
    'fetchTemplateFiles',
    {
        markup: '<div id="dsm-frame" data-template="wrapperDesktopStandard" class="cleanslate">' +
                '<form id="dsm-question-meta">' +
                    '<div data-js="dsm-replace-screen"></div>' +
                '</form>' +
                '<div id="dsm-foot">' +
                    '<div id="dsm-progress-info">' +
                        '<div>' +
                            '<button id="dsm-continue-btn">Continue</button>' +
                        '</div>' +
                        '<div>' +
                            '<p id="dsm-question-count">{{question_count}}</p>' +
                        '</div>' +
                    '</div>' +
                    '<div class="sub_logo_block">' +
                        '{{#links.length}}' +
                            '{{#links}}' + 
                                '<a id="info-link" href="{{url}}" target="_blank">{{text}}</a>' +
                            '{{/links}}' + 
                        '{{/links.length}}' +                      
                    '</div>' +
                    '<div class="timer-and-progress">' +
                        '<p id="dsm-survey-timer"></p>' +
                        '<p id="dsm-survey-progress"></p>' +
                    '</div>' +
                '</div>' +
            '</div>'
    }
);