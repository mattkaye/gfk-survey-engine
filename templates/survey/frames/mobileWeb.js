window.dimestoreCallback(
    'fetchTemplateFiles',
    {
        markup: '<div id="dsm-modal-overlay" class="fadeIn animated"></div>' + 
            '<div id="dsm-modal" data-template="wrapperMobileWeb" class="cleanslate bounceInDown animated">' +
                '<a id="dsm-modal-close"></a>' +
                '<form id="dsm-question-meta">' +
                    '<div data-js="dsm-replace-screen"></div>' +
                '</form>' +
                '<div id="dsm-foot">' +
                    '<div id="dsm-progress-info">' +
                        '<div>' +
                            '<button id="dsm-continue-btn">Continue</button>' +
                        '</div>' +
                        '<div>' +
                            '<p id="dsm-question-count">{{question_count}}</p>' +
                        '</div>' +
                    '</div>' +
                    '<div>' +
                        '<div>' +
                            '<img id="dsm-logo" src="{{project.custom_logo}}">' +
                        '</div>' +
                    '</div>' +
                    '<div class="sub_logo_block">' +
                        '{{#links.length}}' +
                            '{{#links}}' + 
                                '<a id="info-link" href="{{url}}" target="_blank">{{text}}</a>' +
                            '{{/links}}' + 

                            '{{^links.url}}' + 
                                '<a id="info-link" href="http://dimestore.com/SurveyPrivacyPolicy.html" target="_blank">Privacy Policy</a>' +
                            '{{/links.url}}' + 
                        '{{/links.length}}' +                      
                    '</div>' +
                    '<div class="timer-and-progress">' +
                        '<p id="dsm-survey-timer"></p>' +
                        '<p id="dsm-survey-progress"></p>' +
                    '</div>' +
                '</div>' +
            '</div>'
    }
);