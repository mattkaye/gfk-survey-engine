window.dimestoreCallback(
    'fetchTemplateFiles',
    {
        markup: '<div id="dsm-modal" data-template="wrapperInApp" class="cleanslate">' +
                '<a id="dsm-modal-close"></a>' +
                '<form id="dsm-question-meta">' +
                    '<div data-js="dsm-replace-screen"></div>' +
                '</form>' +
                '<div id="dsm-foot">' +
                    '<div id="dsm-progress-info">' +
                        '<div>' +
                            '<button id="dsm-continue-btn">Continue</button>' +
                        '</div>' +
                        '<div>' +
                            '<p id="dsm-question-count">{{question_count}}</p>' +
                        '</div>' +
                    '</div>' +
                    '<div>' +
                        '<div>' +
                            '<p id="dsm-survey-timer"></p>' +
                        '</div>' +
                    '</div>' +
                    '<div>' +
                        '<div>' +
                            '{{privacy}}' +
                        '</div>' +
                        '<div>' +
                            '{{logo}}' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>'
    }
);
