window.dimestoreCallback(
    'fetchTemplateFiles',
    {
        markup: '<div id="container" data-template="registration">' +
            '<div id="register" style="background:#{{background_color}} !important;">' +
                '<div class="row">' +
                    '<div class="small-12 column text-center register-text">' +
                        '<h2 style="color:#{{text_color}} !important;">{{message}}</h2>' +
                    '</div>' +
                '</div>' +
                '<div class="row">' +
                    '<div class="small-12 column text-center">' +
                        '<input type="email" name="respondentEmail" value="" placeholder="{{textbox_message}}" data-email-address>' +
                    '</div>' +
                '</div>' +
                '<div>' +
                    '<div class="row register_buttons">' +
                        '{{#hide_skip_button}}' +
                        '<div>' +
                            '<button data-action="register">{{submit_button_text}}</button>' +
                        '</div>' +
                        '{{/hide_skip_button}}' +

                        '{{^hide_skip_button}}' +
                        '<div class="fl">' +
                            '<button data-action="register">{{submit_button_text}}</button>' +
                        '</div>' + 
                        '<div class="fr">' +
                            '<button data-action="skip">Skip</button>' +
                        '</div>' +
                        '{{/hide_skip_button}}' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>'
    }
);
