window.dimestoreCallback(
    'fetchTemplateFiles',
    {
        markup: '<div id="container" data-template="endScreenDefault" data-name="endScreen">' +
            '<div class="row">' +
                '<div class="small-12 column text-center" id="end_screen">' +
                    '<h1>Thank You!</h1>' +
                    '<h2>Your opinion matters</h2>' +
                '</div>' +
            '</div>' +
        '</div>'
    }
);
