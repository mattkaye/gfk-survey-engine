window.dimestoreCallback(
    'fetchTemplateFiles',
    {
        markup: '<div id="container" data-template="demo">' +
            '<div class="row">' +
                '<div class="small-12 column" id="question">' +
                    '<h2>{{text}}</h2>' +
                '</div>' +
            '</div>' +
            '<div class="row">' +
                '<div class="small-12 column" id="answers">' +
                    '<input type="hidden" name="questionID" value="{{id}}">' +
                    '<input type="hidden" name="questionType" value="demo">' +
                    '<input type="hidden" name="questionCategory" value="{{questionCategory}}"> {{#questions}}' +
                    '<label for="questionID_{{id}}">{{text}}</label>' +
                    '<select id="questionID_{{id}}" name="answerID">' +
                        '{{#answers}}' +
                        '<option value="{{id}}" data-default="{{is_default_answer}}">{{label}}</option>' +
                        '{{/answers}}' +
                    '</select>' +
                    '{{/questions}}' +
                '</div>' +
            '</div>' +
        '</div>'
    }
);
