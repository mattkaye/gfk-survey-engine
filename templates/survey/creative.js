window.dimestoreCallback(
    'fetchTemplateFiles',
    {
        markup: '<div id="container" data-template="creative">' +
             '<div class="row">' +
                '{{#click_url}}' +
	            	'<a id="creative_click" data-action="creative_click" href="{{{click_url}}}" target="_blank">' +
	            '{{/click_url}}' +
                    
                    '{{#isHTML}}' +
                        '<iframe id="dsm-html5-creative" class="frame-{{creativeHeight}}-high" src="{{{url}}}" scrolling="no">' + 
                    '{{/isHTML}}' +
                    
                    '{{^isHTML}}' +
                	   '<img src="{{{url}}}">' +
                    '{{/isHTML}}' +
                        
                '{{#click_url}}' +
	            	'</a>' +
	            '{{/click_url}}' +
            '</div>' +
        '</div>'
    }
);