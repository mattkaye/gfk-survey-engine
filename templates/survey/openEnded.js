window.dimestoreCallback(
    'fetchTemplateFiles',
    {
        markup: '<div id="container" data-template="open">' +
            '<div class="row">' +
                '<div class="small-12 column" id="question">' +
                    '<h2>{{text}}</h2>' +
                '</div>' +
            '</div>' +
            '<div class="row" id="char_count">' +
                '<div class="small-12 column">' +
                    '<p>Characters remaining: <span id="characters_left">{{char_limit}}</span></p>' +
                '</div>' +
            '</div>' +
            '<div class="row">' +
                '<div class="small-12 column" id="answers">' +
                    '<input type="hidden" name="questionID" value="{{id}}">' +
                    '<input type="hidden" name="questionType" value="open">' +
                    '<input type="hidden" name="questionCategory" value="{{questionCategory}}">' +
                    '<textarea name="oeAnswerText" id="oeAnswerText"></textarea>' +
                '</div>' +
            '</div>' +
        '</div>'
    }
);
