window.dimestoreCallback(
    'fetchTemplateFiles',
    {
        markup: '<div id="container" data-template="endScreenCustom" data-name="endScreen">' +
            '{{#click_url}}' +
            '<a id="endscreen_click" data-action="endscreen_click" href="{{{click_url}}}" target="_blank">' +
            '{{/click_url}}' +

                '{{#noEndScreen}}' +
                    '<div class="row">' +
                        '<div class="small-12 column text-center" id="end_screen">' +
                            '<h1>Thank You!</h1>' +
                            '<h2>Your opinion matters</h2>' +
                        '</div>' +
                    '</div>' +
                '{{/noEndScreen}}' +
                
                '{{#isHTML}}' +
                    '<iframe id="dsm-html5-creative" class="frame-{{creativeHeight}}-high" src="{{{url}}}" scrolling="no">' + 
                '{{/isHTML}}' +
                    
                '{{^isHTML}}' +
                   '{{^noEndScreen}}' +
                        '<img id="custom_end_screen" src="{{{url}}}">' +
                   '{{/noEndScreen}}' +
                '{{/isHTML}}' +
                
            '{{#click_url}}' +
            '</a>' +
            '{{/click_url}}' +
        '</div>'
    }
);
