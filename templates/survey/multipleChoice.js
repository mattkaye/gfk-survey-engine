window.dimestoreCallback(
    'fetchTemplateFiles',
    {
        markup: '<div id="container" data-template="multi">' +
            '<div class="row">' +
                '<div class="small-12 column" id="question">' +
                    '<h2>{{text}}</h2>' +
                '</div>' +
            '</div>' +
            '<div class="row">' +
                '<div class="small-12 column" id="answers">' +
                    '<input type="hidden" name="questionID" value="{{id}}">' +
                    '<input type="hidden" name="questionType" value="multi">' +
                    '<input type="hidden" name="questionCategory" value="{{questionCategory}}">' +
                    '<ul class="no-bullet">' +
                    '{{#answers}}' +
                        '<li>' +
                           '<input type="checkbox" name="answerID" value="{{id}}" id="answer_{{id}}" data-exclusive="{{mutually_exclusive}}">' +
                            '<label for="answer_{{id}}">{{text}}</label>' +
                        '</li>' +
                    '{{/answers}}' +
                    '</ul>' +
                '</div>' +
            '</div>' +
        '</div>'
    }
);
