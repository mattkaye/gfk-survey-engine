window.dimestoreCallback(
    'fetchTemplateFiles', {
        markup: '<div id="container" data-template="one">' +
            '<div class="row">' +
                '<div id="question">' +
                      '<h2>{{text}}</h2>' +
                '</div>' +
            '</div>' +
            '<div class="row">' +
              '<div id="answers">' +
                  '<input type="hidden" name="questionID" value="{{id}}">' +
                  '<input type="hidden" name="questionType" value="one">' +
                  '<input type="hidden" name="questionCategory" value="{{questionCategory}}">' +
                  '<ul class="no-bullet">' +
                  '{{#answers}}' +
                        '<li>' +
                              '<input type="radio" name="answerID" value="{{id}}" id="answer_{{id}}">' +
                              '<label for="answer_{{id}}">{{text}}</label>' +
                        '</li>' +
                  '{{/answers}}' +
                  '</ul>' +
              '</div>' +
            '</div>' +
            '</div>'
    }
);