/**
 @fileOverview Sets up the proper path values based on the environment the framework is running in.

 @module Environment
 @author Matt Kaye
 */
define([], function () {
    'use strict';

    return function () {
        /**
        * Retrieve the URL paths for all the service end points. 
        
        * @returns {Object} an object with the URL paths for all the service end points. <em>(CDN, API, PIXEL, COOKIES)</em>
        */
        this.getEnvironmentPath = function () {
            var environmentPath,
                tagEnvironment,
                allScripts,
                scriptPath,
                pattern,
                scriptName,
                isSecured = false,
                i,
                isFilePath;

            allScripts = document.getElementsByTagName("script");
            for (i = 0; i < allScripts.length; i++) {
                scriptName = allScripts[i].getAttribute("src");
                isFilePath = scriptName !== null && scriptName !== "";
                if (isSecured === false && isFilePath === true) {
                    isSecured = this.isSecuredPath(scriptName);
                }
                pattern = /(api|stagingapi2|devapi)-dimestore.dmi.sensic.net/i;
                if (pattern.test(allScripts[i].getAttribute("src"))) {
                    scriptPath = allScripts[i].getAttribute("src");
                    tagEnvironment = 'PROD';
                    if (scriptPath.indexOf('stagingapi2') > -1) {
                        tagEnvironment = 'STAGE';
                    } else if (scriptPath.indexOf('devapi') > -1) {
                        tagEnvironment = 'DEV';
                    } else {
                        tagEnvironment = false;
                    }
                    break;
                }
            }
            tagEnvironment = window.location.hostname;

            switch (tagEnvironment) {
            case 'PROD':
                environmentPath = {
                    'CDN': '//content-dimestore.dmi.sensic.net/prod/',
                    'API': '//api-dimestore.dmi.sensic.net/vi-api/',
                    'PIXEL': '//pixel-dimestore.dmi.sensic.net/vi-pixel/',
                    'COOKIES': '//mobileapp-dimestore.dmi.sensic.net/mobile-utility/'
                };
                break;

            case 'STAGE':
                environmentPath = {
                    'CDN': '//stagingadmin2-dimestore.dmi.sensic.net/prod/',
                    'API': '//stagingapi2-dimestore.dmi.sensic.net/vi-api/',
                    'PIXEL': '//stagingpixel2-dimestore.dmi.sensic.net/vi-pixel/',
                    'COOKIES': '//staging-mobileapp-dimestore.dmi.sensic.net/mobile-utility/'
                };
                console.log('Running in environment: Staging');
                break;

            case 'DEV':
                environmentPath = {
                    'CDN': '//devadmin-dimestore.dmi.sensic.net/prod/',
                    'API': '//devapi-dimestore.dmi.sensic.net/vi-api/',
                    'PIXEL': '//devpixel-dimestore.dmi.sensic.net/vi-pixel/',
                    'COOKIES': '//devpixel-dimestore.dmi.sensic.net/mobile-utility'
                };
                console.log('Running in environment: Development');
                break;

            default:
                environmentPath = {
                    'CDN': '//stagingadmin2-dimestore.dmi.sensic.net/prod/',
                    'API': '//stagingapi2-dimestore.dmi.sensic.net/vi-api/',
                    'PIXEL': '//pixel-dimestore.dmi.sensic.net/vi-pixel/',
                    'COOKIES': '//mobileapp-dimestore.dmi.sensic.net/mobile-utility/',
                    'LOCAL_INSTALL_PATH': '//localhost/frontend-framework/'
                };
                // console.log('Running in environment: Custom');
                // console.log('%c Remember to set your LOCAL_INSTALL_PATH in the Environment module ', 'background:#FDC687;');
                break;
            }
            if (isSecured === true) {
                environmentPath.CDN = "https:" + environmentPath.CDN;
                environmentPath.API = "https:" + environmentPath.API;
                environmentPath.PIXEL = "https:" + environmentPath.PIXEL;
                environmentPath.COOKIES = "https:" + environmentPath.COOKIES;
            }
            return environmentPath;
        };
        this.isSecuredPath = function (scriptName) {
            if (scriptName.indexOf('dimestore.dmi.sensic.net') > -1 && scriptName.indexOf('https') > -1) {
                return true;
            }
            return false;
        };
    };
});