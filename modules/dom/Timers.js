/**
 @fileOverview Methods tasked with JavaScript timers.

 @module DOM/Timers
 @author Matt Kaye
 @requires DOM
 */
define(function (require) {
    'use strict';

    var parentModule,
        $ = require('jquery');

    return function (parent) {
        parentModule = parent;
        /**
        * Delay moving to the next screen by X seconds.
        
        * @param {Integer} seconds Number of seconds to delay moving to the next screen.
        */
        this.delaySurveyBySeconds = function (seconds) {
            setTimeout(function () {
                parentModule.update.moveToNextScreen();
                parentModule.update.restoreSurveyScrollBars();
            }, seconds * 1000);
        };

        /**
        * Delay removing an animation class name by X seconds.
        
        * @param {Object} domNode The node to be targeted.
        * @param {String} animationClass Name of the class to remove.
        * @param {Integer} seconds Seconds to delay the action.
        */
        this.removeAnimationClass = function (domNode, animationClass, seconds) {
            setTimeout(
                function () {
                    $(domNode).removeClass(animationClass);
                },
                seconds * 1000
            );
        };

        /**
        * Delay the function and callback by X seconds.
        
        * @param {Integer} seconds Seconds to delay the action.
        * @param {function} callback Callback function.
        */
        this.delayFunctionCall = function (seconds, callback) {
            setTimeout(function () {
                callback();
            }, seconds * 1000);
        };
    };
});