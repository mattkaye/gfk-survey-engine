/**
 @fileOverview Methods tasked with updating DOM nodes.

 @module DOM/Update
 @author Matt Kaye
 @requires DOM
 @requires Data
 @requires Utility
 @requires API
 @requires Timers
 @requires Tags
 */
define(function (require) {
    'use strict';

    var $ = require('jquery'),
        UtilityModule = require('../utility/Utility'),
        DataModule = require('../data/Data'),
        ApiModule = require('../api/Api'),
        ListenModule = require('./Listen'),
        TimersModule = require('./Timers'),
        CookieModule = require('../cookie/Cookie'),
        TagsModule = require('../tags/Tags');


    return function (parent) {
        var utility = UtilityModule,
            data = new DataModule(),
            api = new ApiModule(),
            listen = new ListenModule(),
            timers = new TimersModule(),
            cookie = new CookieModule(),
            tags = new TagsModule();

        this.parent = parent;

        /**
         * Move the survey forward by one screen.
         */

        this.moveToNextScreen = function () {
            var surveyScreens = data.getPersistentSurveyData('DSM_SURVEY_SCREENS'),
                delaySurveyTime,
                replaceContainer = $('[data-js="dsm-replace-screen"]'),
                cookiePath,
                tagType = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.type;

            delaySurveyTime = data.getPersistentSurveyData('DSM_SURVEY_CHOSEN_CREATIVE') ? data.getPersistentSurveyData('DSM_SURVEY_CHOSEN_CREATIVE').duration : false;

            $(replaceContainer).html(surveyScreens[0]);
            this.showSurveyFooter();
            if (utility.bool.isLastScreen() || utility.bool.isCreativeScreen()) {
                this.removeSurveyScrollBars();
                this.hideSurveyFooter();
                if (utility.bool.isCreativeScreen()) {
                    timers.delaySurveyBySeconds(delaySurveyTime);
                    listen.bindCreativeClickEvent(listen.creativeScreenClickListener);
                }
                if (utility.bool.isEndScreen()) {
                    cookiePath = cookie.setCookies.buildCookieParam('cookie');
                    parent.create.writeScriptTagToHead(cookiePath, 'dsm_script_tag');
                    timers.delayFunctionCall(3, this.hideSurvey);
                    listen.bindEndScreenClickEvent(listen.endScreenClickListener);

                    if (tagType === 'Flyover') {
                        timers.delayFunctionCall(5, tags.mobileWeb.modalAnimateClose);
                    }
                }
            }

            if (utility.bool.showSurveyProgress()) {
                this.updateSurveyProgress();
            }

            this.updateSurveyTimer();
            data.removeLastUsedSurveyScreen();

            $(replaceContainer).data('survey-started', true);
            return;
        };

        /**
         
         * Go to a defined survey end screen.
         */

        this.goToEndscreen = function () {
            var surveyScreens = data.getPersistentSurveyData('DSM_SURVEY_SCREENS'),
                replaceContainer = $('[data-js="dsm-replace-screen"]'),
                exposure = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.exposure,
                tagType = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.type,
                isSurveyComplete = data.getPersistentSurveyData('SURVEY_COMPLETE_FLAG'),
                cookiePath;

            if (exposure === 'exposed') {
                timers.delayFunctionCall(3, this.hideSurvey);
            }
            if (exposure === 'control' && isSurveyComplete === true) {
                data.setPersistentSurveyData('SURVEY_COMPLETE_FLAG', false);

                timers.delayFunctionCall(3, this.hideSurvey);
            }

            $(replaceContainer).html(surveyScreens[surveyScreens.length - 1]);

            // End screen click event handler.
            $('[data-js="dsm-replace-screen"]').on('click', '[id="endscreen_click"]', function () {
                listen.endScreenClickListener();
            });
            $('#dsm-foot').remove();
            this.removeSurveyScrollBars();
            this.hideSurveyFooter();

            if (tagType === 'Flyover') {
                timers.delayFunctionCall(5, tags.mobileWeb.modalAnimateClose);
            }
            cookiePath = cookie.setCookies.buildCookieParam('cookie');
            parent.create.writeScriptTagToHead(cookiePath, 'dsm_script_tag');
            return;
        };

        /**
         * Makes a checkboxes exclusive on multiple answer question types.
         */
        this.makeCheckboxesExclusive = function () {
            var checkBoxes = $('[data-js="dsm-replace-screen"] input:checkbox'),
                i;

            for (i = 0; i < checkBoxes.length; i++) {
                if ($(checkBoxes[i]).data('exclusive') === false || $(checkBoxes[i]).prop('checked') === false) {
                    $(checkBoxes[i]).prop({
                        'checked': 0,
                        'disabled': 1
                    });
                }
            }
            return;
        };

        /**
         * Start the animation for a validation error.
         * @param {String} animationName Any valid animation type name. See {@tutorial Animation}.
         */
        this.triggerValidationAnimation = function (animationName) {
            var surveyFrame = $('[data-template]');
            $(surveyFrame).removeClass('bounceInDown');
            $(surveyFrame).addClass(animationName);
            timers.removeAnimationClass(surveyFrame, animationName, 1);
            return;
        };

        /**
         * Updates the survey progress bar, showing what question the user is on compared with the total question count.
         */

        this.updateSurveyProgress = function () {
            var questionData = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.questions,
                surveyLength = questionData.length,
                currentQuestionNumber,
                surveyProgressNode = $('#dsm-survey-progress'),
                currentProgress = $(surveyProgressNode).text();

            currentQuestionNumber = currentProgress.split('/')[0] || 0;
            currentQuestionNumber = parseInt(currentQuestionNumber, 10) + 1;

            data.setPersistentSurveyData('CURRENT_QUESTION_NUMBER', currentQuestionNumber);

            if (utility.bool.isQuestionScreen()) {
                $(surveyProgressNode).text(currentQuestionNumber + '/' + surveyLength);
            }
            return;
        };

        /**
         * Reset the survey progress bar.
         */

        this.resetSurveyProgress = function () {
            var questionData = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.questions,
                surveyLength = questionData.length,
                surveyProgressNode = $('#dsm-survey-progress');

            $(surveyProgressNode).text('0/' + surveyLength);
            return;
        };

        /**
         
         * Update the continue button with custom theme text.
         */

        this.updateContinueButtonText = function (buttonText) {
            $('#dsm-continue-btn').text(buttonText);
        };

        /**
         * Removes the survey scrollbars. Used so there are no scrollbars at end Screen
         */

        this.removeSurveyScrollBars = function () {
            $('#dsm-frame').addClass('no_overflow');
        };

        /**
         * Restore the survey scrollbars.
         */

        this.restoreSurveyScrollBars = function () {
            $('#dsm-frame').removeClass('no_overflow');
        };

        /**
         * Hides the survey footer. Countdown timer, progress, logo, privacy policy.
         */

        this.hideSurveyFooter = function () {
            $('#dsm-foot').removeClass('show').addClass('hide');
        };

        /**
         * Shows the survey footer. Countdown timer, progress, logo, privacy policy.
         */

        this.showSurveyFooter = function () {
            $('#dsm-foot').removeClass('hide').addClass('show');
        };

        /**
         * Updates the survey timer countdown. Also responsible for restarting the survey timer.
         */

        this.updateSurveyTimer = function () {
            var surveyTimerNode = $('#dsm-survey-timer'),
                timer,
                apiParam,
                surveyScreens = data.getPersistentSurveyData('DSM_SURVEY_SCREENS_TEMPLATE'),
                surveyScreenOrder = data.getPersistentSurveyData('DSM_SURVEY_SCREEN_ORDER_TEMPLATE'),
                time = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.survey_info.inactivity_timeout,
                inActivityObj = [],
                questionnaireID = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.survey_info.id,
                tagData,
                inActivityTime = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.inactivityTimeoutValue,
                boolInActivity = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.inactivityTimeout,
                currentQuestionNumber = data.getPersistentSurveyData('CURRENT_QUESTION_NUMBER');

            if (utility.bool.isQuestionScreen()) {
                window.clearInterval(window.DimestoreTimerInterval);
                window.clearInterval(window.inActivitySurvey);
                if (boolInActivity === true && currentQuestionNumber === 1 && inActivityTime > 0) {
                    window.inActivitySurvey = setInterval(function () {
                        inActivityObj.questionnaireID = questionnaireID;
                        inActivityObj.respondentID = data.getPersistentSurveyData('DSM_COOKIES').respondentID;
                        apiParam = api.helper.buildAPIParam('inactivity', inActivityObj);
                        api.post.postToAPI(apiParam);
                        window.clearInterval(window.inActivitySurvey);
                        window.clearInterval(window.DimestoreTimerInterval);
                        parent.update.goToEndscreen();
                        parent.update.hideSurvey();
                    }, inActivityTime * 1000);
                }
                timer = utility.math.surveyTimer({
                    seconds: time,
                    onUpdateStatus: function (remainingTime) {
                        if (utility.bool.showSurveyTimer() && time !== "0") {
                            $(surveyTimerNode).text(remainingTime);
                        }
                    },
                    restartTimer: function () {
                        window.DimestoreTimerInterval = timer.start();
                    },
                    onCounterEnd: function () {
                        if (utility.bool.isQuestionScreen()) {

                            tagData = data.getPersistentSurveyData('DSM_SURVEY_DATA');
                            apiParam = api.helper.buildAPIParam('surveyTimeout', tagData);
                            if (apiParam.event === "surveyTimeout") {
                                delete apiParam.exposure;
                            }
                            api.post.postToAPI(apiParam);


                            if (utility.bool.surveyWillLoop()) {
                                data.setPersistentSurveyData('DSM_SURVEY_SCREENS', surveyScreens);
                                data.setPersistentSurveyData('DSM_SURVEY_SCREEN_ORDER', surveyScreenOrder);
                                if (utility.bool.showSurveyProgress()) {
                                    parent.update.resetSurveyProgress();
                                }
                                parent.update.moveToNextScreen();
                            } else {
                                parent.update.goToEndscreen();
                            }
                        }
                    }
                });
                window.DimestoreTimerInterval = timer.start();
            }
            return;
        };

        /**
         * Hide the survey and display the Ads.
         */
        this.hideSurvey = function () {
            var surveyAdData,
                type = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.type;

            surveyAdData = window.surveyAdData;
            if (surveyAdData !== undefined) {
                surveyAdData.objActualAd.style.display = "block";
                $(surveyAdData.adParentNode).html(surveyAdData.objActualAd);
            }
            if (type === 'Flyover') {
                $('#dsm-modal').parent().hide();
            }

        };

        /**
         * Hide the Creative for overlay
         * @param {object} objAdObject
         * @returns {object} adParent HTML node object
         */
        this.hideCreative = function (objAdObject) {
            var objActualAd = objAdObject,
                adParent,
                arrAllObjects,
                i;

            adParent = objActualAd.parentNode;
            $(objActualAd).hide();

            if (undefined !== objActualAd.className && "" !== objActualAd.className) {
                arrAllObjects = document.getElementsByClassName(objActualAd.className);
            }
            if (undefined !== arrAllObjects) {
                for (i = 0; i < arrAllObjects.length; i++) {
                    $(arrAllObjects[i]).hide();
                }
            }
            return adParent;
        };
    };
});