define([], function () {
    'use strict';

    return function (parent) {
        this.parent = parent;
    };
});