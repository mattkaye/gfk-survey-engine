/**
 @fileOverview Methods tasked with reading DOM nodes.
 @module DOM/Read
 @author Matt Kaye
 @requires DOM
 */
define(function (require) {
    'use strict';
    var $ = require('jquery');

    return function (parent) {

        this.parent = parent;
        /**
         * Given a file name, find the script node in the DOM.
         
         * @param {String} fileName The name of the file without the .js extension.
         * @returns {Mixed} Either the found DOM node or false.
         */
        this.findScriptTagByFileName = function (fileName) {
            var foundDomNode,
                allScriptTags,
                foundScriptFileName,
                i;

            fileName += '.js';
            allScriptTags = document.getElementsByTagName('script');

            for (i = 0; i < allScriptTags.length; i++) {
                foundScriptFileName = allScriptTags[i].src.replace(/\?.*$/, "").replace(/.*\//, "");

                if (foundScriptFileName === fileName) {
                    foundDomNode = allScriptTags[i];
                    break;
                }
            }
            return foundDomNode || false;
        };

        /**
         * Determine if the current script running has parent DOM access.
         
         * @returns {Boolean}
         */
        this.canAccessParentDOM = function () {
            return (window.top === window.self ? true : false);
        };

        /**
         * Determine if the site is running on secure protocol.
         
         * @returns {Boolean}
         */
        this.siteIsSecure = function () {
            return window.location.protocol === 'https:';
        };

        /**
         * Given a script tag node, return all the URL parameters.
         
         * @returns {Mixed} A string of tag parameters or false.
         */
        this.getTagParameters = function (scriptTagNode) {
            return scriptTagNode.src.split('?')[1] || false;
        };

        /**
         * Determine if the users cookies are enabled.
         
         * @returns {Boolean}
         */
        this.cookiesAreEnabled = function () {
            var cookieEnabled;

            cookieEnabled = (navigator.cookieEnabled) ? true : false;
            if (navigator.cookieEnabled === "undefined" && cookieEnabled === false) {
                document.cookie = "cookiesEnabledTest";
                cookieEnabled = (document.cookie.indexOf("cookiesEnabledTest") !== -1) ? true : false;
            }
            return cookieEnabled ? true : false;
        };

        /**
         * Find the Ads/Creative on page for overlay tag and return the Parent node and Ads/Creative html node.
         
         * @returns {object}
         */
        this.findCreative = function (attemptCount, tagData) {
            var surveyAdData = {};

            if (tagData.isLive === false || tagData.previewMode === "true") {

                if (tagData.actualAdObject === undefined) {
                    surveyAdData = this.getCreativeObject(tagData, attemptCount);
                } else {
                    surveyAdData.adParentNode = this.parent.update.hideCreative(tagData.actualAdObject);
                }
            }

            return surveyAdData;
        };
        /**
         * Gets the creative object.
         * 
         * @param {object} tagData
         * @param {integer} attemptCount
         * @returns {object} surveyAdData Object of Actual creative node and parent node.
         */
        this.getCreativeObject = function (tagData, attemptCount) {
            var objDSMBookMark,
                objParent,
                creativeObjData = {},
                attemptCountConstant,
                delayFuntionCallBy;

            attemptCountConstant = 6;
            delayFuntionCallBy = 0.5;

            objDSMBookMark = document.getElementById("dsmObject" + tagData.tagId);
            objParent = objDSMBookMark.parentNode;
            creativeObjData.objActualAd = this.findCreativeNode(objParent, tagData);

            if ((creativeObjData.objActualAd === false) && (attemptCountConstant >= attemptCount)) {
                this.parent.timers.delayFunctionCall(delayFuntionCallBy, this.findCreative(delayFuntionCallBy, tagData));
            } else {
                tagData.actualAdObject = creativeObjData.objActualAd;
                creativeObjData.adParentNode = this.parent.update.hideCreative(creativeObjData.objActualAd);
            }

            return creativeObjData;
        };
        /**
         * Gets the actual creative object by matching dimension.
         
         * @param {object} objNode HTML node object.
         * @param {object} tagData Tag data object.
         * @returns {object}
         */
        this.findCreativeNode = function (objNode, tagData) {
            var i,
                objNodeChildren;

            objNodeChildren = $(objNode).find('*');
            for (i = 0; i < objNodeChildren.length; i++) {
                if (objNodeChildren[i].width !== undefined && objNodeChildren[i].height !== undefined) {

                    if (parseInt(objNodeChildren[i].width, 10) === tagData.bgWidth &&
                            parseInt(objNodeChildren[i].height, 10) === tagData.bgHeight) {
                        return objNodeChildren[i];
                    }
                }
            }
            return false;
        };
    };
});