/**
 @fileOverview Methods tasked with DOM node creation.

 @module DOM/Create
 @author Matt Kaye
 @requires DOM
 @requires Environment
 @requires jQuery
 */
define(function (require) {
    'use strict';
    var EnvironmentModule = require('../environment/Environment'),
        $ = require('jquery');

    return function (parent) {
        this.parent = parent;

        var environment;
        environment = new EnvironmentModule();

        /**
        * Write tracking pixels to the document.
        
        * @param {Array} pixelsArray The array of pixel IDs to write as image tags.
        */
        this.writePixelsToDOM = function (pixelsArray) {
            var i,
                pixelPath,
                pixels = [];

            pixelPath = environment.getEnvironmentPath().PIXEL;

            for (i = 0; i < pixelsArray.length; i++) {
                pixels.push('<img src="' + pixelPath + '?action=pixel&event=pixel&pixelID=' + pixelsArray[i] + '" data-js="dimestore_pixel" width="1" height="1">');
            }
            $('body').append(pixels.join(''));
        };

        /**
        * Write a script tag to the head of a document.
        
        * @param {String} scriptPath The path to the script you wish to call.
        * @param {String} tagID An ID attribute assigned to the script tag node.
        */
        this.writeScriptTagToHead = function (scriptPath, tagID) {
            var head,
                script;

            if (tagID) {
                $('#' + tagID).remove();
            }

            head = document.getElementsByTagName('head')[0];
            script = document.createElement('script');
            script.type = 'text/javascript';
            script.id = tagID;
            script.src = scriptPath;
            head.appendChild(script);
        };

        /**
        * Write CSS stylesheets to the head of a document.
        
        * @param {Array} pathToFiles An array of one or more file paths.
        */
        this.writeStylesheetToHead = function (pathToFiles) {
            var i;

            for (i = 0; i < pathToFiles.length; i++) {
                $('head').append('<link rel="stylesheet" href="' + pathToFiles[i] + '" type="text/css" />');
            }
        };

        /**
        * Write inline CSS styles to the head of a document.
        
        * @param {String} cssStyles CSS styles rules that are to be added.
        */
        this.writeStylesToHead = function (cssStyles) {
            var head = document.head,
                style = document.createElement('style');

            style.type = 'text/css';
            if (style.styleSheet) {
                style.styleSheet.cssText = cssStyles;
            } else {
                style.appendChild(document.createTextNode(cssStyles));
            }
            head.appendChild(style);
        };
    };
});