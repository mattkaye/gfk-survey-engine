/**
 @fileOverview All methods involving DOM manipulation.

 @module DOM
 @author Matt Kaye
 */
define(function (require, exports) {
    'use strict';
    var DomCreateModule = require('./Create'),
        DomReadModule = require('./Read'),
        DomUpdateModule = require('./Update'),
        DomRemoveModule = require('./Remove'),
        DomListenerModule = require('./Listen'),
        DomTimerModule = require('./Timers');

    exports.create = new DomCreateModule(this);
    exports.read = new DomReadModule(this);
    exports.update = new DomUpdateModule(this);
    exports.remove = new DomRemoveModule(this);
    exports.listen = new DomListenerModule(this);
    exports.timers = new DomTimerModule(this);

});