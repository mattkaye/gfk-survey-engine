/**
 @fileOverview It decides whether user is eligible for survey or not based on its Global invite/Project invite / Percent invite. Also it decides if user is elligble for survey then which survey tag need to serve to user i.e Control or expose tag.

 @module Decisioning
 @author Santosh Rathod
 @requires Utility
 @requires jQuery
 */
define(function (require) {
    'use strict';
    var $ = require('jquery'),
        DataModule = require('../data/Data'),
        UtilityModule = require('../utility/Utility'),
        utility = UtilityModule;

    return function () {
        var data = new DataModule();

        /**
        * Checks survey eligibility for user.
        
        * @param {object} payload Request payload object.
        * @returns {boolean} Return whether survey is invited or not (true/false).
        */
        this.checkSurveyEligibility = function (payload) {
            var isInvited = false,
                studyType,
                config = {},
                surveyData,
                key,
                eligibilityConstant;

            eligibilityConstant = {
                projectType : {
                    CE: 'CE',
                    CI: 'CI'
                },
                invitationType: {
                    global: 'globalInvitation',
                    project: 'projectInvitations',
                    percent: 'percentInvite'
                }
            };

            surveyData = data.getPersistentSurveyData('DSM_SURVEY_DATA');
            config = surveyData.JS_TAG_PARAMETERS;
            for (key in payload) {
                if (payload.hasOwnProperty(key)) {
                    config[key] = payload[key];
                }
            }
            config.hasCompletedSurvey = utility.bool.hasCompletedSurvey(config.pid, config.dmisrvy_project_complete);
            config = this.checkPixelExposureCount(config, (config.studyType === 1) ? false : true);
            config = this.checkGlobalInvite(config);

            if (config.globalInvitations.invite) {
                config = this.checkProjectInvite(config);
                if (config.tagInvitations.invite) {
                    studyType = (config.studyType === 1) ? eligibilityConstant.projectType.CI : eligibilityConstant.projectType.CE;
                    config = this.checkPercentageInvite(config, studyType);
                    if (config.percentInvite.invite) {
                        isInvited = true;
                    } else {
                        config.invitationType = eligibilityConstant.invitationType.percent;
                    }
                } else {
                    config.invitationType = eligibilityConstant.invitationType.project;
                }
            } else {
                config.invitationType = eligibilityConstant.invitationType.global;
            }

            surveyData.JS_TAG_PARAMETERS = config;
            data.setPersistentSurveyData('DSM_SURVEY_DATA', surveyData);
            window['data_' + config.tagId] = surveyData.JS_TAG_PARAMETERS;

            return isInvited;
        };
        /** 
         * Checks for global invite of servey if available and set global invitation value into payload config and returns config.
         
         * @param {object} config Request payload object
         * @returns {object} Returns config object with global invitation value.
         */
        this.checkGlobalInvite = function (config) {

            var globalInvite = config.dmisrvy_global_invite,
                objGlobalInviteVals = [],
                result = {};

            result.returnVal = true;
            config.globalInvite = globalInvite;

            if (globalInvite === undefined || globalInvite === null) {
                result.returnVal = true;
            } else if (config.globalInvitations.maxInvitations === 0) {
                result.returnVal = false;
            } else {
                objGlobalInviteVals = globalInvite.split(',');
                result = this.checkInviteValObj(objGlobalInviteVals, config, 'globalInvitations');
                config.globalInvitations.value = result.arrRelevantVals.join(',') || '';
            }

            if (result.returnVal) {
                result.arrRelevantVals[result.arrRelevantVals.length] = config.invitationValue;
                config.globalInvitations.invite = true;
            } else {
                config.globalInvitations.invite = false;
            }

            return config;
        };
        /** 
         * Checks for Tag invitation of servey if available and set project invite value into payload config and returns config.
         
         * @param {object} config Request payload object.
         * @returns {object} Returns config object with project invitation value.
         */
        this.checkProjectInvite = function (config) {
            var projectInvite = config.dmisrvy_project_invite,
                strCookieVal,
                inviteValObj,
                result = {};

            result.returnVal = true;

            if (projectInvite === '' || projectInvite.indexOf(config.pid) === -1) {
                result.returnVal = true;
            } else if (config.tagInvitations.maxInvitations === 0) {
                result.returnVal = false;
            } else {
                strCookieVal = this.getProjectCookieValue(projectInvite, config.pid);
                config.projectInvite = strCookieVal;
                inviteValObj = strCookieVal.split(',');
                result = this.checkInviteValObj(inviteValObj, config, 'tagInvitations');
            }
            if (result.returnVal) {
                if (result.arrRelevantVals) {
                    if (result.arrRelevantVals.length > 99) {
                        result.arrRelevantVals.splice(0, result.arrRelevantVals.length - 99);
                    }
                }
                config.tagInvitations.invite = true;
            } else {
                config.tagInvitations.invite = false;
            }
            return config;

        };

        /*
         * Gets the Project invite value from project cookie.
         * 
         * @param {string} projectInvite
         * @param {string} projectID
         * @returns {String}
         */
        this.getProjectCookieValue = function (projectInvite, projectID) {
            var objProjectInviteVals = projectInvite.split('|'),
                strCookieVal,
                cookieObj;

            $.each(objProjectInviteVals, function (idx, val) {
                idx = idx || false;
                if (val.indexOf(projectID) !== -1) {
                    strCookieVal = val;
                }
            });
            cookieObj = strCookieVal.split(':');

            return cookieObj[1];
        };

        /**    
         * Checks for percent invite of servey if available and
         * set percent invite value into payload config and returns config. Also sets the exposure (exposed/control) parameter based on percent invite.
         
         * @param {object} config Request payload object.
         * @param {string} studyType Study type whether CE/CI.
         * @returns {object} Returns config object with percent invite value.
         */
        this.checkPercentageInvite = function (config, studyType) {
            var percentageInvite = config.percentInvite,
                siteId = utility.string.decodeString(config.siteId),
                siteSpecificVal = percentageInvite[siteId],
                returnVal,
                invite;

            config.percentInvite.invite = 'true';

            if (siteSpecificVal === undefined) {
                siteSpecificVal = percentageInvite['default'];
            }

            if (studyType === 'CE') {
                config = this.setExposureValue(config);
                config.peCount = parseInt(config.peCount, 10);

                if (config.peCount === -1) {
                    config.peCount = '0';
                }

                invite = config.exposure === 'exposed' ? siteSpecificVal.invite_expose : siteSpecificVal.invite_control;

            } else {
                invite = siteSpecificVal.invite;
            }

            returnVal = this.doInviteRandomization(invite, config);
            config.percentInvite.invite = returnVal;

            return config;

        };
        /**  
         * Checks for pixel cookie value and based on pixel cookie value returns the exposure (exposed/control).
         
         * @param {object} config Request payload object.
         * @returns {string} Returns Exposure as exposed or control.
         */
        this.checkPixelExposure = function (config) {
            var returnExposure = "control",
                pixelCookieVal,
                arrPixels = config.pixels,
                i;

            for (i in arrPixels) {
                if (arrPixels.hasOwnProperty(i)) {
                    pixelCookieVal = parseInt(config['pixel_' + arrPixels[i]], 10);
                    if (!isNaN(pixelCookieVal) && pixelCookieVal > 0) {
                        returnExposure = 'exposed';
                    }
                }
            }
            return returnExposure;
        };
        /** 
         * Check for percent invite value and based on that randomly assign exposure value as control/exposed.
         
         * @param {object} config Request payload object.
         * @returns {string} Returns Exposure as exposed or control.
         */
        this.randomlyAssignControlExpose = function (config) {
            var exposure,
                percentageInvite = config.percentInvite,
                siteSpecificVal = percentageInvite[config.siteId],
                percentControl,
                randomnumber;

            if (undefined === siteSpecificVal) {
                siteSpecificVal = percentageInvite['default'];
            }
            percentControl = siteSpecificVal.percent_control;
            randomnumber = Math.floor(Math.random() * 101);
            exposure = randomnumber < percentControl ? 'control' : 'exposed';

            return exposure;
        };
        /** 
         * Check for percent invite value and based on that randomly assign exposure value as control/exposed.
         
         * @param {object} invite
         * @param {object} config Request payload object.
         * @returns {string} Returns bReturnVal.
         */
        this.doInviteRandomization = function (invite, config) {
            var bReturnVal = true,
                randomnumber;

            if (invite === 0) {
                randomnumber = 0;
                bReturnVal = false;
            } else if (invite === 100) {
                randomnumber = 100;
                bReturnVal = true;
            } else {
                randomnumber = Math.floor(Math.random() * 101);
                bReturnVal = randomnumber < invite ? true : false;
            }

            config.value = randomnumber;
            return bReturnVal;
        };
        /** 
         * Checks for invite value object i.e Global invite and project invite and returns the array relevant value for global/project invite and return value as boolean.
         
         * @param {Array} inviteValObj Global invite value or project invite value.
         * @param {object} config Request payload object
         * @param {string} type invitation type(globalInvitations, tagInvitation)
         * @returns {object} Returns object consisting returnVal and Array of relevant value of global or project invite.
         */

        this.checkInviteValObj = function (inviteValObj, config, type) {

            var diff,
                eligibilityHours,
                arrRelevantVals = [],
                returnVal;
            $.each(inviteValObj, function (idx, val) {
                idx = idx || false;
                if (null !== val && undefined !== val) {
                    diff = config.invitationValue - parseInt(val, 10);
                    eligibilityHours = config[type].maxDaysInHours * 60 * 60 * 1000; /*Converting to miliseconds*/
                    if (diff <= eligibilityHours) {
                        arrRelevantVals[arrRelevantVals.length] = val;
                    }
                }
            });

            if ((arrRelevantVals.length === 0) || ((arrRelevantVals.length > 0) &&
                (arrRelevantVals.length < config[type].maxInvitations))) {
                returnVal = true;
            } else {
                returnVal = false;
            }

            return {
                'returnVal': returnVal,
                'arrRelevantVals': arrRelevantVals
            };
        };
        /**   
         * Sets exposure value and return config with exposure value.
         
         * @param {object} config Request payload object
         * @returns {object} Returns config object with exposure value.
         */
        this.setExposureValue = function (config) {
            var cookieExposureVal,
                pixelExposureVal,
                exposureVal;

            cookieExposureVal = config.peCount === -1 ? '' : config.exposure;
            pixelExposureVal = this.checkPixelExposure(config);
            exposureVal = pixelExposureVal === 'exposed' ? pixelExposureVal : cookieExposureVal;

            if ('exposed' !== exposureVal && 'control' !== exposureVal) {
                exposureVal = this.randomlyAssignControlExpose(config);
            }

            config.exposure = exposureVal;
            return config;
        };

        /**   
         * Gets the Pixel Exposure count
         
         * @param {object} config tagData object
         * @param {boolean} bAssignExposure true/false
         * @returns {object} Returns config updated config with peCount value.
         */
        this.checkPixelExposureCount = function (config, bAssignExposure) {
            var objAllProject = config.dmisrvy_project_data,
                iExposureCount;

            if (objAllProject !== undefined) { //cookie not present                
                iExposureCount = this.getPECountFromProjectData(config) !== undefined ? this.getPECountFromProjectData(config) : -1;
            }

            config.peCount = iExposureCount;
            if (bAssignExposure) {
                if (iExposureCount === 0) {
                    config.exposure = 'control';
                }
                if (iExposureCount > 0) {
                    config.exposure = 'exposed';
                }
            }
            return config;
        };

        /**
         * Gets the project exposure count from project data
         * 
         * @param {type} config
         * @returns {integer} Project Exposure count
         */
        this.getPECountFromProjectData = function (config) {
            var projectArray = config.dmisrvy_project_data.split(','),
                i,
                projectData;

            for (i = 0; i < projectArray.length; i++) {
                if (projectArray[i].indexOf(config.pid) !== -1) {
                    projectData = projectArray[i].split(':');
                    if (projectData[1] !== '' && projectData[1] !== undefined) {
                        return parseInt(projectData[1], 10);
                    }
                }
            }
        };
    };
});