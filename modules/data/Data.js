/**
 @fileOverview Methods that are primarily used for querying and manipulation of object data.

 @module Data
 @author Matt Kaye
 @requires Environment
 */
define(function (require) {
    'use strict';
    var EnvironmentModule = require('../environment/Environment'),
        storage = require('storage'),
        $ = require('jquery');

    return function () {
        var environment = new EnvironmentModule();

        /**
        * Builds up an object that holds all the details about the survey.
        
        * @param {String} ProjectID The project ID.
        * @param {String} tagID The tag ID.
        * @param {Function} callback A user defined callback function that will capture the value of the survey Object.
        */
        this.getSurveyJSON = function (projectID, tagID, callback) {
            var tagJsonPath,
                surveyJsonPath,
                themePath,
                isSecured,
                allTagData = {};

            tagJsonPath = environment.getEnvironmentPath().CDN + 'tags/v3/' + projectID + '/' + tagID + '.js';
            surveyJsonPath = environment.getEnvironmentPath().CDN + 'survey_data/' + projectID + '/' + projectID + '.json';

            $.when(
                $.get(tagJsonPath, function () {
                    allTagData.JS_TAG_PARAMETERS = window['data_' + tagID];
                }),
                $.get(surveyJsonPath, function (result) {
                    allTagData.SURVEY = result;
                })
            ).then(function () {
                themePath = allTagData.SURVEY.multi_size === 'true' ? allTagData.JS_TAG_PARAMETERS.themePath : allTagData.SURVEY.project_info.theme_data_path;
                isSecured = environment.isSecuredPath(surveyJsonPath);
                if (isSecured === true) {
                    themePath = themePath.replace('http', 'https');
                }
                $.get(themePath, function (result) {
                    allTagData.THEME = result;
                    callback(allTagData);
                });
            });
        };

        /**
        * Get stored survey data.
        
        * @param {String} dataKey The data key of the object you want to fetch.
        * @returns {Object} See {@tutorial Survey Data Object}.
        */
        this.getPersistentSurveyData = function (dataKey) {
            return storage.get(dataKey);
        };

        /**
        * Set stored survey data.
        
        * @param {String} dataKey The data key of the object you want to set.
        * @param {String} data The data you will be adding to the object.
        * See {@tutorial Survey Data Object}.
        */
        this.setPersistentSurveyData = function (dataKey, data) {
            storage.set(dataKey, data);
        };

        /**
        * Remove stored  data.
        
        * @param {String} dataKey The data key of the object you want to remove.
        
        */
        this.removePersistentSurveyData = function (dataKey) {
            storage.remove(dataKey);
        };

        /**
        * Remove stored survey data.
        
        * @param {String} dataKey The data key of the object you want to remove.
        * See {@tutorial Survey Data Object}.
        */
        this.removePersistentSurveyData = function (dataKey) {
            storage.remove(dataKey);
        };

        /**
         * Removes the first element of two local storage arrays, DSM_SURVEY_SCREENS and DSM_SURVEY_SCREEN_ORDER. These keep the questions in order.
         */
        this.removeLastUsedSurveyScreen = function () {
            var surveyScreens = this.getPersistentSurveyData('DSM_SURVEY_SCREENS'),
                screenTypes = this.getPersistentSurveyData('DSM_SURVEY_SCREEN_ORDER');

            surveyScreens.shift();
            screenTypes.shift();

            if (surveyScreens.length === 0) {
                this.removePersistentSurveyData('DSM_SURVEY_SCREENS');
                this.removePersistentSurveyData('DSM_SURVEY_SCREEN_ORDER');
            } else {
                this.setPersistentSurveyData('DSM_SURVEY_SCREENS', surveyScreens);
                this.setPersistentSurveyData('DSM_SURVEY_SCREEN_ORDER', screenTypes);
            }
            return;
        };

        /**
        * Get the maximum character limit for open ended questions.
        
        * @param {Array} questions An array of questions to search through.
        * @returns {Integer} Maximum character limit.
        */
        this.getOpenTextQuestionMaxCharacterCount = function (questions) {
            var i,
                characterLimit = 256;

            for (i = 0; i < questions.length; i++) {
                if (questions[i].char_limit) {
                    characterLimit = parseInt(questions[i].char_limit, 10);
                    break;
                }
            }
            return characterLimit;
        };

        /**
        * Get the exposure type for the survey.
        
        * @returns {Mixed} Survey exposure type or false if none is defined.
        */
        this.getExposureType = function () {
            return this.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.exposure || false;
        };
    };
});