/**
 @fileOverview Methods for setting up the Cookie operations.

 @module Cookie/SetCookie
 @author Santosh Rathod
 @requires jQuery
 @requires Data
 @requires Environment
 */
define(function (require) {
    'use strict';
    var $ = require('jquery'),
        DataModule = require('../data/Data'),
        UtilityModule = require('../utility/Utility'),
        EnvironmentModule = require('../environment/Environment');

    return function () {

        var data = new DataModule(),
            environment = new EnvironmentModule();

        /**
        * Given an API event name, the matching Cookie action is found and returned with the parameters required in setting up the cookie.
        
        * @param {String} event The event name.
        * @returns {String} mergedAPIParameters A String that holds the parameters required in the particular event. 
        */

        this.buildCookieParam = function (event) {
            var cookiePath,
                tagData,
                cookieObject,
                utility = UtilityModule,
                registrationData,
                CookieEndpoint,
                maxInvite;
            CookieEndpoint = environment.getEnvironmentPath().COOKIES;
            cookieObject = data.getPersistentSurveyData('DSM_COOKIES');
            tagData = window['data_' + data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.tagId];

            switch (event) {
            case 'requested':
                cookiePath = CookieEndpoint + '/setCookies.jsp?respondentID=' + cookieObject.respondentID + '&respondentEmail=' + cookieObject.respondentEmail;
                break;

            case 'answer':
                if (data.getPersistentSurveyData('SURVEY_COMPLETE_FLAG')) {
                    cookiePath = CookieEndpoint + '/setCookies.jsp?projectId=' + tagData.pid;
                }
                break;

            case 'cookie':
                if (data.getPersistentSurveyData('SURVEY_COMPLETE_FLAG')) {
                    cookiePath = CookieEndpoint + '/setCookies.jsp?projectId=' + tagData.pid + '&ProjectExposureCount=';
                    if (tagData.exposure === "control" && data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.type !== 'InBanner') {
                        cookieObject.peCount = cookieObject.peCount === -1 ? '' : cookieObject.peCount;
                        cookieObject.peCount = parseInt(cookieObject.peCount + 1, 10);
                        cookiePath += cookieObject.peCount;
                    }

                    data.setPersistentSurveyData('DSM_COOKIES', cookieObject);
                    data.setPersistentSurveyData('SURVEY_COMPLETE_FLAG', false);
                }
                break;

            case 'projectStart':

                cookiePath = CookieEndpoint + '/setCookies.jsp?invitationValue=' + cookieObject.invitationValue + '&projectId=' + tagData.pid + '&projectExposureCount=';
                if (!(data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.type === 'InBanner') && cookieObject.peCount > -1) {
                    cookiePath += cookieObject.peCount;
                } else {
                    cookiePath += cookieObject.peCount >= 0 ? cookieObject.peCount : '';
                }
                maxInvite = utility.bool.checkSurveyMaxInvite();
                if (maxInvite) {
                    cookiePath += '&maxInviteLimit=true';
                }
                data.setPersistentSurveyData('DSM_COOKIES', cookieObject);
                break;

            case 'registration':
                registrationData = utility.obj.urlStringToObject($('#dsm-question-meta').serialize());
                cookiePath = CookieEndpoint + '/setCookies.jsp?respondentID=' + cookieObject.respondentID + '&respondentEmail=' + registrationData.respondentEmail;
                break;

            default:
                break;
            }

            return cookiePath;
        };
    };
});