/**
 @fileOverview Methods that get and set cookie values.

 @module Cookie
 @author Santosh Rathod
 */
define(function (require) {
    'use strict';
    var SetCookiesModule = require('./SetCookie');

    return function () {
        this.setCookies = new SetCookiesModule(this);
    };
});