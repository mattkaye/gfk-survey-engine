/**
 @fileOverview Mobile web flyover specific methods.

 @module Tags/MobileWeb
 @author Matt Kaye
 */
define(function (require) {
    'use strict';

    var $ = require('jquery');

    return function () {
        var inviteOverlay,
            creativeNodeFound;

        /**
        * Set up the overlay and button for the mobile invite
        
        * @param {Object} creativeNode DOM node for the creative to overlay.
        * @param {String} surveyInviteText The text on the invite button.
        * @param {String} animationEffect The animation the button takes see {@tutorial Animation}.
        */
        this.setupSurveyInvite = function (creativeNode, surveyInviteText, animationEffect) {
            creativeNodeFound = creativeNode;
            inviteOverlay = $('#dsmInviteOverlay');

            $('body').append('<div id="dsmInviteOverlay"></div>');
            $('#dsmInviteOverlay').html('<div id="dsmInviteButton" class="animated ' + animationEffect + '">' + surveyInviteText + '</div>');

            this.sizeSurveyInvite();
            return;
        };

        /**
         * Event Listener for the trigger to close the survey modal
         */
        this.modalCloseListener = function () {
            //Bind to the closing button
            var _this = this;
            $(document).on('click', '#dsm-modal-close', function () {
                _this.modalAnimateClose();
                return false;
            });
        };

        /**
         * The animation to close the survey modal
         */
        this.modalAnimateClose = function () {
            var modalOverlay = $('#dsm-modal-overlay'),
                modalWindow = $('#dsm-modal'),
                modalFrame = $(modalWindow).parent();

            $(modalOverlay).addClass('fadeOut');
            $(modalWindow).addClass('bounceOutUp');

            modalOverlay.one('webkitAnimationEnd oanimationend msAnimationEnd animationend',
                function () {
                    inviteOverlay = $('#dsmInviteOverlay');
                    $(inviteOverlay).remove();
                    modalFrame.hide();
                    window.clearInterval(window.DimestoreTimerInterval);
                });
        };

        /**
         * Event Listener for device orientation changes. This moves the invite overlay to be on top of the creative on orientation change.
         */
        this.sizeSurveyInvite = function () {
            if (creativeNodeFound) {
                var creativeDimensions = creativeNodeFound.getBoundingClientRect(),
                    creativeOffset = $(creativeNodeFound).offset();

                $('#dsmInviteOverlay').css({
                    width: creativeDimensions.width,
                    height: creativeDimensions.height,
                    top: creativeOffset.top,
                    left: creativeOffset.left
                }).show();
            }
        };

        window.addEventListener('deviceorientation', this.sizeSurveyInvite);
    };
});