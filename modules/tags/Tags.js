/**
 @fileOverview Methods that deal with manipulation of tag level data.

 @module Tags
 @author Matt Kaye
 */
define(function (require) {
    'use strict';
    var HelperModule = require('./Helper'),
        MobileWeb = require('./MobileWeb');

    return function () {
        this.helper = new HelperModule(this);
        this.mobileWeb = new MobileWeb(this);
    };
});