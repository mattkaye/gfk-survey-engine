/**
 @fileOverview General helpers methods for the Tags module.

 @module Tags/Helper
 @author Matt Kaye
 @requires Theme
 @requires Data
 */
define(function (require) {
    'use strict';
    var $ = require('jquery'),
        EnvironmentModule = require('../environment/Environment'),
        DataModule = require('../data/Data'),
        DomModule = require('../dom/Dom'),
        UtilityModule = require('../utility/Utility'),
        ThemeModule = require('../theme/Theme');

    return function (parent) {
        var environment = new EnvironmentModule(),
            data = new DataModule(),
            dom = DomModule,
            utility = UtilityModule,
            theme = new ThemeModule(),
            baseAssetURL = (environment.getEnvironmentPath().LOCAL_INSTALL_PATH || environment.getEnvironmentPath().CDN);

        /**
        * Get the type of tag the survey is running
        
        * @param {Object} tagData Data for this tag.
        * @returns {Object} tagType Object containing the target user platform, survey type, and DOM wrapper name for this survey.
        */
        this.getTagType = function (tagData) {
            var tagType;

            tagData = tagData || data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS;
            tagData.playerVersion = parseInt(tagData.playerVersion, 10);
            if (tagData.playerVersion === 4) {
                tagType = {
                    targetPlatform: 'mobile web',
                    type: 'flyover',
                    wrapperName: 'wrapperMobileWeb'
                };
            } else if (tagData.playerVersion === 5) {
                switch (tagData.type) {
                case 'InBanner':
                    tagType = {
                        targetPlatform: 'desktop',
                        type: 'in banner',
                        wrapperName: 'wrapperDesktopStandard'
                    };
                    break;

                case 'Overlay':
                    tagType = {
                        targetPlatform: 'desktop',
                        type: 'overlay',
                        wrapperName: 'wrapperDesktopStandard'
                    };
                    break;

                case 'Flyover':
                    tagType = {
                        targetPlatform: 'desktop',
                        type: 'flyover',
                        wrapperName: 'wrapperDesktopFlyover'
                    };
                    break;
                }
            }
            return tagType;
        };

        /**
        * Get an array of survey template names in the order they will appear in the survey
        
        * @param {Object} jsonData Project level JSON data.
        * @returns {Array} surveyScreenOrder An array of screen names the survey will be comprised of in the correct order.
        */
        this.getSurveyViewTemplateNamesInOrder = function (jsonData) {
            var i,
                surveyScreenOrder = [];

            if (jsonData.creatives) {
                surveyScreenOrder.push('creative');
            }

            if (utility.bool.userHasNotRegistered() && jsonData.registration) {
                if (jsonData.registration.enabled === 'true' && jsonData.project_info.reg_before_quiz === 'true') {
                    surveyScreenOrder.push('registration');
                }
            }

            for (i = 0; i < jsonData.questions.length; i++) {
                surveyScreenOrder.push(jsonData.questions[i].type);
            }

            if (utility.bool.userHasNotRegistered() && jsonData.registration) {
                if (jsonData.registration.enabled === 'true' && jsonData.project_info.reg_before_quiz === 'false') {
                    surveyScreenOrder.push('registration');
                }
            }

            if (jsonData.endscreens) {
                surveyScreenOrder.push('endScreenCustom');
            } else {
                surveyScreenOrder.push('endScreenDefault');
            }
            return surveyScreenOrder;
        };

        /**
        * Gets the CSS class name based on the survey dimensions. 
        
        * @param {Integer|String} width Survey width.
        * @param {Integer|String} height Survey height.
        * @returns {String} className The CSS class name.
        */
        this.getSurveyDimensionsClass = function (width, height) {
            var className = 'size_' + width + 'x' + height;
            return className;
        };

        /**
        * Load tag specific dependencies based on the target platform the survey is running on. 
        
        * @param {Object} tagObject Project level JSON data.
        * @param {Function} callback Function to run when all dependencies have loaded.
        */
        this.loadTagDependencies = function (tagObject, callback) {
            dom.create.writeStylesheetToHead([
                baseAssetURL + 'bowerComponents/cleanslate/cleanslate.css',
                baseAssetURL + 'assets/common/css/common.css'
            ]);

            switch (tagObject.targetPlatform) {
            case 'mobile web':
                var customThemeCSS = theme.css.buildCustomMobileWebCSS(),
                    surveyNodeID,
                    inviteButtonText,
                    inviteDelay,
                    delaySurveyBySeconds,
                    completedProjects,
                    hasTakenSurvey,
                    projectId,
                    inviteWindow,
                    creativeNode;

                dom.create.writeStylesheetToHead([
                    baseAssetURL + 'assets/mobileWeb/css/main.css',
                    baseAssetURL + 'bowerComponents/animate.css/animate.min.css'
                ]);

                dom.create.writeStylesToHead(customThemeCSS);

                if (utility.bool.scriptParameterIsDefined('inApp')) {
                    //Stand alone survey URL
                    dom.listen.surveyStartListener();
                    callback();
                } else {
                    inviteDelay = data.getPersistentSurveyData('DSM_SURVEY_DATA').THEME.project.invite_overlay.delay || 1;
                    inviteWindow = data.getPersistentSurveyData('DSM_SURVEY_DATA').THEME.project.invite_overlay.window || 10;
                    inviteButtonText = theme.helper.getInviteButtonOverlay().buttonText;
                    surveyNodeID = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.tagId;
                    creativeNode = document.getElementById(surveyNodeID).previousElementSibling;

                    //Delay showing the invite by X seconds
                    setTimeout(function () {
                        parent.mobileWeb.setupSurveyInvite(creativeNode, inviteButtonText, 'rubberBand');

                        $('#dsmInviteButton').one('click', function () {
                            var continueButtonText = theme.helper.getContiueButtonStyles().buttonText || 'Continue';
                            dom.listen.surveyStartListener();
                            callback();
                            dom.update.updateContinueButtonText(continueButtonText);
                        });

                        //Delay removing the invite by X seconds
                        setTimeout(function () {
                            var inviteOverlay = $('#dsmInviteOverlay');
                            $(inviteOverlay).remove();
                        }, inviteWindow * 1000);
                    }, inviteDelay * 1000);
                }
                parent.mobileWeb.modalCloseListener();
                break;
            case 'desktop':
                projectId = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.pid;
                completedProjects = data.getPersistentSurveyData('DSM_COOKIES').dmisrvy_project_complete;
                hasTakenSurvey = utility.bool.hasCompletedSurvey(projectId, completedProjects.split(','));
                customThemeCSS = theme.css.buildCustomDesktopCSS();
                hasTakenSurvey = utility.bool.hasCompletedSurvey(projectId, completedProjects.split(','));

                dom.create.writeStylesToHead(customThemeCSS);

                dom.create.writeStylesheetToHead([
                    baseAssetURL + 'assets/desktop/css/main.css',
                    baseAssetURL + 'assets/desktop/css/surveyDimensions.css'
                ]);

                if (utility.bool.oneSurveyPerUser() === false || hasTakenSurvey === false) {
                    dom.listen.surveyStartListener();
                }

                delaySurveyBySeconds = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.delay || 0;

                if (data.getExposureType() === 'control') {
                    delaySurveyBySeconds = 0;
                }

                if (tagObject.type === 'flyover') {
                    dom.create.writeStylesheetToHead([
                        baseAssetURL + 'assets/desktop/css/flyover.css',
                        baseAssetURL + 'bowerComponents/animate.css/animate.min.css'
                    ]);
                    setTimeout(function () {
                        callback();
                        parent.mobileWeb.modalCloseListener();
                    }, delaySurveyBySeconds * 1000);
                } else {
                    setTimeout(function () {
                        callback();
                    }, delaySurveyBySeconds * 1000);
                }
                break;
            }
        };
    };
});