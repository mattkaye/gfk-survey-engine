/**
 @fileOverview Post methods for API operations.

 @module API/Post
 @author Matt Kaye
 @requires API
 @requires Environment
 @requires jQuery
 */
define(function (require) {
    'use strict';
    var EnvironmentModule = require('../environment/Environment'),
        $ = require('jquery'),
        DataModule = require('../data/Data');

    return function () {
        var environment = new EnvironmentModule(),
            data = new DataModule();

        /**
        * Post data to the API endpoint.
        
        * @param {Object} userSubmittedPayload Object data being sent to the API.
        * @param {Function=} callback A user defined function that will fire once the POST is complete.
        * @returns {Object} If a callback function is defined, the result of the API call will be the first parameter of the callback function. 
        */
        this.postToAPI = function (userSubmittedPayload, callback) {
            var apiEndpoint,
                previewMode = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.previewMode;

            apiEndpoint = environment.getEnvironmentPath().API;
            if (previewMode === 'true') {
                apiEndpoint = this.getAPIPathForPreview(userSubmittedPayload.event);
                $.get(apiEndpoint + '?' + $.param(userSubmittedPayload), function (result) {
                    if (callback) {
                        callback(result);
                    }
                });
                return;
            }
            userSubmittedPayload = $.param(userSubmittedPayload);
            $.post(apiEndpoint + '?' + userSubmittedPayload, function (result) {
                if (callback) {
                    callback(result);
                }
            });
        };
        /**
         * Post data to the Integration endpoint.
         * @param {String} userSubmittedURL String of the Integration URL's
         * @param {Function=} callback A user defined function that will fire once the POST is complete.
         * @returns {Object} If a callback function is defined, the result of the API call will be the first parameter of the callback function.
         */
        this.postIntegration = function (userSubmittedURL, callback) {
            $.ajax({
                type: "POST",
                url: userSubmittedURL,
                success: callback,
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                }
            });
        };
        this.getAPIPathForPreview = function (event) {
            var apiPath = environment.getEnvironmentPath().CDN + 'event/apiResponse.json';

            if (event === 'requested') {
                apiPath = environment.getEnvironmentPath().CDN + 'event/requested.js';
            }
            return apiPath;
        };
    };
});