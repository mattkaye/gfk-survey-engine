/**
 @fileOverview API methods that send and receive data from the server endpoints.

 @module API
 @author Matt Kaye
 @see https://confluence.gfk.com/download/attachments/82879439/API%20Data%20Points.xlsx?api=v2
 */
define(function (require) {
    'use strict';
    var PostModule = require('./Post'),
        HelperModule = require('./Helper');
    return function () {
        this.post = new PostModule(this);
        this.helper = new HelperModule(this);
    };
});