/**
 @fileOverview Methods that deal with setting up the survey views.

 @module Views
 @author Matt Kaye
 @requires Tags
 @requires Mustache
 @requires Utility
 @requires Data
 */
define(function (require) {
    'use strict';

    var mustache = require('../../bowerComponents/mustache/mustache.min'),
        UtilityModule = require('../utility/Utility'),
        TagsModule = require('../tags/Tags'),
        DataModule = require('../data/Data');

    return function () {
        var utility = UtilityModule,
            tags = new TagsModule(),
            data = new DataModule();

        /**
        * Set up the overlay and button for the mobile invite
        
        * @param {Array} templateTypes Template type names.
        * @return {Object} surveyTemplatesInOrder Key value pairs consisting of template name : template file path.
        */
        this.getTemplateFileNames = function (templateTypes) {
            var i,
                surveyTemplatesInOrder = [];

            for (i = 0; i < templateTypes.length; i++) {
                surveyTemplatesInOrder.push(this.mapTemplateTypeToFile(templateTypes[i]));
            }
            return surveyTemplatesInOrder;
        };

        /**
        * Mapping from template name to template file path.
        
        * @param {String} templateName A template name.
        * @return {Object} matchingTemplatePair Key value pair consisting of template name : template file path.
        */
        this.mapTemplateTypeToFile = function (templateName) {
            var baseTemplatePath,
                matchingTemplatePair = {},
                templateMapping = {},
                key;

            baseTemplatePath = 'templates/survey';

            templateMapping = {
                'wrapperMobileWeb': baseTemplatePath + '/frames/mobileWeb.js',
                'wrapperInApp': baseTemplatePath + '/frames/mobileInApp.js',
                'wrapperDesktopStandard': baseTemplatePath + '/frames/desktopStandard.js',
                'wrapperDesktopFlyover': baseTemplatePath + '/frames/desktopFlyover.js',
                'creative': baseTemplatePath + '/creative.js',
                'registration': baseTemplatePath + '/registration.js',
                'one': baseTemplatePath + '/singleChoice.js',
                'multi': baseTemplatePath + '/multipleChoice.js',
                'open': baseTemplatePath + '/openEnded.js',
                'demo': baseTemplatePath + '/demographic.js',
                'endScreenDefault': baseTemplatePath + '/endScreenDefault.js',
                'endScreenCustom': baseTemplatePath + '/endScreenCustom.js'
            };

            key = templateName;
            matchingTemplatePair[key] = templateMapping[templateName];
            return matchingTemplatePair;
        };

        /**
        * Renders Mustache tags to markup in survey template screens.
        
        * @param {Array} surveyTemplates An array of survey screen markup.
        * @param {Object} surveyData Survey data to populate into the screen markup in place of mustache templates.
        * @return {Array} renderedSurveyScreens An array of fully rendered survey screens.
        */
        this.renderSurveyScreens = function (surveyTemplates, surveyData) {
            var renderedSurveyScreens = [],
                thisTemplate,
                questionCountIndex,
                thisQuestionsAnswers,
                endScreenObject,
                endScreenMeta,
                exposureType,
                creativeIDsArray,
                chosenCreative,
                creativeGroupName,
                consumerInsightCreativeGroupNames,
                selectedCreativeGroupName,
                theTruth = true,
                i;

            questionCountIndex = 0;
            for (i = 0; i < surveyTemplates.length; i++) {
                thisTemplate = surveyTemplates[i];
                switch (theTruth) {
                case thisTemplate.indexOf('data-template="creative"') > -1:
                    creativeGroupName = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.creativeGroupName || false;
                    exposureType = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.exposure;

                    if (exposureType === 'control') {
                        creativeIDsArray = surveyData.JS_TAG_PARAMETERS.controlCreativeId.split(',');
                    } else if (exposureType === 'exposed') {
                        if (creativeGroupName === false) {
                            if (surveyData.JS_TAG_PARAMETERS.exposedCreativeId) {
                                creativeIDsArray = surveyData.JS_TAG_PARAMETERS.exposedCreativeId.split(',');
                            } else {
                                creativeGroupName = surveyData.JS_TAG_PARAMETERS.exposedCreativeGroupNames.split(',');
                                creativeIDsArray = utility.obj.getCreativeIDsByGroup(creativeGroupName);
                            }
                        } else {
                            //Select exposed creative by group name
                            creativeGroupName = creativeGroupName.split(',');
                            creativeIDsArray = utility.obj.getCreativeIDsByGroup(creativeGroupName);
                        }
                    } else {
                        if (surveyData.JS_TAG_PARAMETERS.exposedCreativeId) {
                            //Exposure parameter other than control or exposed
                            creativeIDsArray = surveyData.JS_TAG_PARAMETERS.exposedCreativeId.split(',');
                        } else {
                            //Consumer Insight Tag single creative
                            if (surveyData.JS_TAG_PARAMETERS.consumerCreativeId) {
                                creativeIDsArray = surveyData.JS_TAG_PARAMETERS.consumerCreativeId.split(',');
                            } else {
                                //Select exposed creative by group name
                                consumerInsightCreativeGroupNames = creativeGroupName.split(',');
                                creativeIDsArray = utility.obj.getCreativeIDsByGroup(consumerInsightCreativeGroupNames);
                            }
                        }
                    }

                    if (creativeIDsArray.length > 1) {
                        chosenCreative = utility.obj.getRandomCreative(creativeIDsArray);
                    } else {
                        chosenCreative = utility.obj.getCreativeByID(creativeIDsArray.join());
                    }

                    //If don't show creative is not selected
                    if (creativeIDsArray.indexOf('-1') === -1) {
                        if (chosenCreative.type === 'HTML') {
                            chosenCreative.isHTML = true;
                        }
                        renderedSurveyScreens.push(mustache.render(surveyTemplates[i], chosenCreative));
                    }
                    break;

                case thisTemplate.indexOf('data-template="registration"') > -1:
                    surveyData.SURVEY.registration.hide_skip_button = surveyData.SURVEY.registration.hide_skip_button === "true" ? true : false;
                    renderedSurveyScreens.push(mustache.render(surveyTemplates[i], surveyData.SURVEY.registration));
                    break;

                case thisTemplate.indexOf('data-template="endScreenCustom"') > -1:
                    if (utility.bool.manageEndscreensByGroup()) {
                        if (data.getPersistentSurveyData('DSM_SURVEY_CHOSEN_CREATIVE')) {
                            selectedCreativeGroupName = data.getPersistentSurveyData('DSM_SURVEY_CHOSEN_CREATIVE').group_name;
                        } else {
                            if (surveyData.JS_TAG_PARAMETERS.creativeGroupName !== false) {
                                selectedCreativeGroupName = 'backup_for_all_groups';
                            } else {
                                selectedCreativeGroupName = surveyData.JS_TAG_PARAMETERS.exposure;
                            }
                        }
                        if (selectedCreativeGroupName) {
                            endScreenMeta = utility.obj.getEndscreenMeta(selectedCreativeGroupName);
                        } else {
                            endScreenMeta = utility.obj.getRandomEndscreenMeta();
                        }

                        endScreenObject = utility.obj.getEndscreenByProperty(endScreenMeta, selectedCreativeGroupName);
                    } else {
                        endScreenObject = tags.helper.getTagType().targetPlatform === 'desktop' ? surveyData.SURVEY.endscreens.backup_for_all_groups : surveyData.SURVEY.endscreens.endscreen;
                    }
                    if (endScreenObject.type === 'HTML') {
                        endScreenObject.isHTML = true;
                    }
                    renderedSurveyScreens.push(mustache.render(surveyTemplates[i], endScreenObject));
                    break;

                case thisTemplate.indexOf('data-template="endScreenDefault"') > -1:
                    renderedSurveyScreens.push(mustache.render(surveyTemplates[i], {}));
                    break;

                case thisTemplate.indexOf('data-template="wrapperMobileWeb"') > -1:
                case thisTemplate.indexOf('data-template="wrapperInApp"') > -1:
                case thisTemplate.indexOf('data-template="wrapperDesktopStandard"') > -1:
                case thisTemplate.indexOf('data-template="wrapperDesktopFlyover"') > -1:
                    renderedSurveyScreens.push(mustache.render(surveyTemplates[i], surveyData.THEME));
                    break;

                default:
                    if (surveyData.SURVEY.questions[questionCountIndex].randomize_answers === 'true') {
                        thisQuestionsAnswers = surveyData.SURVEY.questions[questionCountIndex].answers;
                        surveyData.SURVEY.questions[questionCountIndex].answers = utility.array.shuffleQuestionAnswers(thisQuestionsAnswers);
                    }
                    renderedSurveyScreens.push(mustache.render(surveyTemplates[i], surveyData.SURVEY.questions[questionCountIndex]));
                    questionCountIndex++;
                    break;
                }
            }
            return renderedSurveyScreens;
        };
    };
});