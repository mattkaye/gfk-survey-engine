/**
 @fileOverview jQuery

 @module jQuery
 */

/*
This is our private version of jQuery. It doesn't expose $ or jQuery in the global namespace, hence does not overwrite those variables that may be in use by another script.
*/
define(['jquery'], function (jq) {
    'use strict';
    return jq.noConflict(true);
});