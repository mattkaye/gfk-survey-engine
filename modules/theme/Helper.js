/**
 @fileOverview General helpers methods for the Theme module.

 @module Theme/Helper
 @author Matt Kaye
 @requires Theme
 @requires Data
 */

define(function (require) {
    'use strict';

    var DataModule = require('../data/Data');

    return function () {
        var data = new DataModule();

        /**
        * Gets the invite overlay button text and styles.

        * @returns {Object}
        */
        this.getInviteButtonOverlay = function () {
            var inviteData = data.getPersistentSurveyData('DSM_SURVEY_DATA').THEME.project.invite_overlay;
            return {
                'delay' : inviteData.delay,
                'window' : inviteData.window,
                'buttonText' : inviteData.button_text,
                'buttonColor' : inviteData.button_color,
                'textColor' : inviteData.text_color
            };
        };

        /**
        * Gets the question section styles.

        * @returns {Object}
        */
        this.getQuestionSectionStyles = function () {
            var questionData = data.getPersistentSurveyData('DSM_SURVEY_DATA').THEME.question_container;
            return {
                'textColor' : questionData.text_color,
                'fontName' : questionData.font_name,
                'fontSize' : questionData.font_size
            };
        };

        /**
        * Gets the answer section styles.

        * @returns {Object}
        */
        this.getAnswerSectionStyles = function () {
            var answerData = data.getPersistentSurveyData('DSM_SURVEY_DATA').THEME.answer_container;
            return {
                'textColor' : answerData.text_color,
                'fontName' : answerData.font_name,
                'fontSize' : answerData.font_size
            };
        };

        /**
        * Gets the contiue button text and styles.

        * @returns {Object}
        */
        this.getContiueButtonStyles = function () {
            var buttonData = data.getPersistentSurveyData('DSM_SURVEY_DATA').THEME.continue_button;
            return {
                'backgroundColor' : buttonData.background_color,
                'textColor' : buttonData.text_color,
                'buttonText' : buttonData.text
            };
        };

        /**
        * Gets the background colors and image for the survey.

        * @returns {Object}
        */
        this.getBackgroundColorsAndImage = function () {
            var projectData = data.getPersistentSurveyData('DSM_SURVEY_DATA').THEME.project;
            return {
                'backgroundColor1' : projectData.background_color1 || 'd0ebff',
                'backgroundColor2' : projectData.background_color2 || 'd0ebff',
                'questionBackgroundColor1' : projectData.question_background_color1 || '0987c8',
                'questionBackgroundColor2' : projectData.question_background_color2 || '035e8d',
                'backgroundImageUrl' : projectData.background_image_url || false
            };
        };

        /**
        * Gets the data for the links.

        * @returns {Object}
        */
        this.getPrivacyLink = function () {
            var projectData = data.getPersistentSurveyData('DSM_SURVEY_DATA').THEME.links[0];
            projectData = projectData || {};
            return {
                'textColor' : projectData.text_color || '',
                'linkText' : projectData.text || '',
                'fontSize' : projectData.font_size || '',
                'url' : projectData.url || ''
            };
        };
    };
});