/**
 @fileOverview Methods that build out the survey theme.

 @module Theme
 @author Matt Kaye
 */
define(function (require) {
    'use strict';
    var HelperModule = require('./Helper'),
        CSSModule = require('./CSS');
    return function () {
        this.helper = new HelperModule(this);
        this.css = new CSSModule(this);
    };
});