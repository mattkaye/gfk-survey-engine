/**
 @fileOverview CSS methods for custom themes.

 @module Theme/CSS
 @author Matt Kaye
 @requires Theme
 @requires Utility
 */
define(function (require) {
    'use strict';

    var UtilityModule = require('../utility/Utility');

    return function (parent) {
        var utility = UtilityModule,
            hasBackgroundImage = false,
            backgroundColors = {},
            formattedJSON;

        /**
        * Builds up CSS from Theme JSON for Mobile Web survey.

        * @returns {String} Inline CSS rules.
        */
        this.buildCustomMobileWebCSS = function () {
            var buttonInviteStyles = parent.helper.getInviteButtonOverlay(),
                backgroundStyles = parent.helper.getBackgroundColorsAndImage(),
                questionStyles = parent.helper.getQuestionSectionStyles(),
                answerStyles = parent.helper.getAnswerSectionStyles(),
                buttonStyles = parent.helper.getContiueButtonStyles(),
                linkStyles = parent.helper.getPrivacyLink();

            formattedJSON = {
                "children": {
                    "#dsm-modal": {
                        "attributes": {
                            "background": "linear-gradient(to bottom, #" + backgroundStyles.backgroundColor1 + "  0%, #" + backgroundStyles.backgroundColor2 + " 100%) !important"
                        }
                    },
                    "#dsmInviteButton": {
                        "attributes": {
                            "background": "#" + buttonInviteStyles.buttonColor + " !important",
                            "color": "#" + buttonInviteStyles.textColor + " !important"
                        }
                    },
                    "#dsm-modal #question": {
                        "attributes": {
                            "background": "linear-gradient(to bottom, #" + backgroundStyles.questionBackgroundColor1 + "  0%, #" + backgroundStyles.questionBackgroundColor2 + " 100%) !important"
                        }
                    },
                    "#dsm-modal #question h2": {
                        "attributes": {
                            "font-size": (questionStyles.fontSize ? questionStyles.fontSize + "px !important" : ""),
                            "font-family": "sans-serif !important",
                            "color": "#" + questionStyles.textColor + " !important"
                        }
                    },
                    "#dsm-modal #answers": {
                        "attributes": {
                            "background": "linear-gradient(to bottom, #" + backgroundStyles.backgroundColor1 + "  0%, #" + backgroundStyles.backgroundColor2 + " 100%) !important",
                            "font-family": "sans-serif !important",
                            "font-size": (answerStyles.fontSize ? answerStyles.fontSize + "px !important" : ""),
                            "color": "#" + answerStyles.textColor + " !important"
                        }
                    },
                    "#dsm-modal #answers label, #dsm-modal #char_count p": {
                        "attributes": {
                            "font-family": "sans-serif",
                            "font-size": (answerStyles.fontSize ? answerStyles.fontSize + "px !important" : ""),
                            "color": "#" + answerStyles.textColor + " !important"
                        }
                    },
                    "#dsm-modal #dsm-survey-progress, #dsm-modal #dsm-survey-timer": {
                        "attributes": {
                            "color": "#" + answerStyles.textColor + " !important"
                        }
                    },
                    "#dsm-modal #char_count p, #dsm-modal #end_screen h1, #dsm-modal #end_screen h2": {
                        "attributes": {
                            "color": "#" + answerStyles.textColor + " !important"
                        }
                    },
                    "#dsm-modal button": {
                        "attributes": {
                            "color": "#" + buttonStyles.textColor + " !important",
                            "background": "#" + buttonStyles.backgroundColor + " !important"
                        }
                    },
                    "#dsm-modal #info-link": {
                        "attributes": {
                            "color": '#' + linkStyles.textColor + " !important",
                            "font-size": linkStyles.fontSize + "px !important"
                        }
                    }
                }
            };
            return utility.obj.JSONToCSS(formattedJSON);
        };

        /**
        * Builds up CSS from Theme JSON for Desktop survey.

        * @returns {String} Inline CSS rules.
        */
        this.buildCustomDesktopCSS = function () {
            var backgroundStyles = parent.helper.getBackgroundColorsAndImage(),
                buttonStyles = parent.helper.getContiueButtonStyles(),
                questionStyles = parent.helper.getQuestionSectionStyles(),
                answerStyles = parent.helper.getAnswerSectionStyles(),
                linkStyles = parent.helper.getPrivacyLink();
            hasBackgroundImage = backgroundStyles.backgroundImageUrl || false;

            backgroundColors.question = "linear-gradient(to bottom, #" + backgroundStyles.questionBackgroundColor1 + "  0%, #" + backgroundStyles.questionBackgroundColor2 + " 100%) !important";
            backgroundColors.answer = "linear-gradient(to bottom, #" + backgroundStyles.backgroundColor1 + "  0%, #" + backgroundStyles.backgroundColor2 + " 100%) !important";

            formattedJSON = {
                "children": {
                    "#dsm-frame, .dsm-frame": {
                        "attributes": {
                            "background": backgroundColors.answer,
                            "background-image": hasBackgroundImage ? "url('" + backgroundStyles.backgroundImageUrl + "') !important" : ''
                        }
                    },
                    "#dsm-frame #question": {
                        "attributes": {
                            "background": hasBackgroundImage ? '' : backgroundColors.question
                        }
                    },
                    "#dsm-frame #question h2": {
                        "attributes": {
                            "font-size": (questionStyles.fontSize ? questionStyles.fontSize + "px !important" : ""),
                            "font-family": "sans-serif !important",
                            "color": "#" + questionStyles.textColor + " !important"
                        }
                    },
                    "#dsm-frame #answers": {
                        "attributes": {
                            "background": hasBackgroundImage ? '' : backgroundColors.answer,
                            "font-family": "sans-serif !important",
                            "font-size": (answerStyles.fontSize ? answerStyles.fontSize + "px !important" : ""),
                            "color": "#" + answerStyles.textColor + " !important"
                        }
                    },
                    "#dsm-frame #answers label, #dsm-frame #char_count p": {
                        "attributes": {
                            "font-family": "sans-serif",
                            "font-size": (answerStyles.fontSize ? answerStyles.fontSize + "px !important" : ""),
                            "color": "#" + answerStyles.textColor + " !important"
                        }
                    },
                    "#dsm-frame #dsm-survey-progress, #dsm-frame #dsm-survey-timer": {
                        "attributes": {
                            "color": "#" + answerStyles.textColor + " !important"
                        }
                    },
                    "#dsm-frame #char_count p, #dsm-frame #end_screen h1, #dsm-frame #end_screen h2": {
                        "attributes": {
                            "color": "#" + answerStyles.textColor + " !important"
                        }
                    },
                    "#dsm-frame button": {
                        "attributes": {
                            "color": "#" + buttonStyles.textColor + " !important",
                            "background": "#" + buttonStyles.backgroundColor + " !important"
                        }
                    },
                    "#dsm-frame #info-link": {
                        "attributes": {
                            "color": '#' + linkStyles.textColor + " !important",
                            "font-size": linkStyles.fontSize + "px !important"
                        }
                    }
                }
            };
            return utility.obj.JSONToCSS(formattedJSON);
        };
    };
});