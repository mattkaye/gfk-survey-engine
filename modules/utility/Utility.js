/**
 @fileOverview Contains all low level utility modules.

 @module Utility
 @author Matt Kaye
 */
define(function (require, exports) {
    'use strict';
    var StringModule = require('./String'),
        MathModule = require('./Math'),
        BooleanModule = require('./Boolean'),
        ArrayModule = require('./Array'),
        ObjectModule = require('./Object');

    exports.string = new StringModule(this);
    exports.math = new MathModule(this);
    exports.bool = new BooleanModule(this);
    exports.array = new ArrayModule(this);
    exports.obj = new ObjectModule(this);
});