/**
 @fileOverview Low level utility methods for Boolean values.

 @module Utility/Boolean
 @author Matt Kaye
 */
define(function (require) {
    'use strict';
    var $ = require('jquery'),
        DataModule = require('../data/Data'),
        DomModule = require('../dom/Dom');

    return function (parent) {
        var data = new DataModule(),
            dom = DomModule;

        /**
        * Validate an email address.
        
        * @param {String} Email address to validate.
        * @returns {Boolean}
        */
        this.isValidEmailAddress = function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@\"]+(\.[^<>()\[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        };

        /**
        * Has this user completed this survey already.
        
        * @param {Integer} projectID The project ID.
        * @param {Array} completedProjects Array of this users completed projects.
        * @returns {Boolean} result
        */
        this.hasCompletedSurvey = function (projectID, completedProjects) {
            var result,
                i,
                surveyData = data.getPersistentSurveyData('DSM_SURVEY_DATA');

            result = false;

            for (i = 0; i < completedProjects.length; i++) {
                if (parseInt(completedProjects[i], 10) === parseInt(projectID, 10)) {
                    result = true;
                    surveyData.JS_TAG_PARAMETERS.hasCompletedSurvey = true;
                    data.setPersistentSurveyData('DSM_SURVEY_DATA', surveyData);
                    break;
                }
            }
            return result;
        };

        /**
         * Can the user take the survey more than once.
         
         * @returns {Boolean} result
         */
        this.oneSurveyPerUser = function () {
            var canRepeatSurvey = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.survey_info.allow_repeat;
            return canRepeatSurvey === "true" ? false : true;
        };

        /**
        * Determines if the survey will restart after the timeout interval is reached
        
        * @returns {Boolean}
        */
        this.surveyWillLoop = function () {
            var surveyWillLoop = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.survey_info.loop_on_survey_timeout;
            return JSON.parse(surveyWillLoop);
        };

        /**
        * Determines if an answer is required in order to move past the current question
        
        * @param {String} questionID The question ID
        * @returns {Boolean}
        */
        this.answerIsRequired = function (questionID) {
            var questionData = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.questions,
                i,
                required = false,
                alwaysRequiredQuestionTypes = ['one', 'multi'];

            for (i = 0; i < questionData.length; i++) {
                if (questionData[i].id === questionID) {
                    if ($.inArray(questionData[i].type, alwaysRequiredQuestionTypes) > -1) {
                        required = true;
                        break;
                    }

                    if (questionData[i].required && questionData[i].required === 'true') {
                        required = true;
                        break;
                    }
                }
            }
            return required;
        };

        /**
        * Determines if the user has supplied an answer for the current question.
        
        * @returns {Boolean} result
        */
        this.answerIsGiven = function () {
            var questionFormSerailized = $('#dsm-question-meta').serialize(),
                formObject,
                i,
                selectedDemoAnswers,
                answerGiven = false;

            formObject = parent.obj.urlStringToObject(questionFormSerailized);

            if (formObject.answerID || formObject.oeAnswerText) {
                answerGiven = true;
            }

            if (formObject.questionType === 'demo') {
                selectedDemoAnswers = $('#dsm-question-meta select option:selected');
                for (i = 0; i < selectedDemoAnswers.length; i++) {
                    if ($(selectedDemoAnswers[i]).data('default')) {
                        answerGiven = false;
                        break;
                    }
                }
            }
            return answerGiven;
        };

        /**
        * Determine if the answer passed to the method mutually exclusive.
        
        * @param {Object} selectedAnswer The DOM node of the selected answer.
        * @returns {Boolean}
        */
        this.answerIsExclusive = function (selectedAnswer) {
            return $(selectedAnswer).data('exclusive') || false;
        };

        /**
        * Determine if the question progress meter should be shown.
        
        * @returns {Boolean}
        */
        this.showSurveyProgress = function () {
            var surveyInfo = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.survey_info;
            return surveyInfo.quiz_meter_visible === "true" ? true : false;
        };

        /**
        * Determine if the survey timer is active and should be shown.
        
        * @returns {Boolean}
        */
        this.showSurveyTimer = function () {
            var surveyInfo = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.survey_info;
            return surveyInfo.inactivity_timeout_show_counter === "true" ? true : false;
        };

        /**
        * Determine if the current screen is a question screen.
        
        * @returns {Boolean}
        */
        this.isQuestionScreen = function () {
            var templateType = $('[data-js="dsm-replace-screen"] [data-template]').data('template'),
                questionTypes = ['one', 'multi', 'open', 'demo'];

            return $.inArray(templateType, questionTypes) > -1 ? true : false;
        };

        /**
        * Determine if this is a multi size survey.
        
        * @returns {Boolean}
        */
        this.isMultiSizeSurvey = function () {
            var surveyData = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.multi_size;
            return surveyData === "true" || false;
        };

        /**
        * Determine if any of the answers passed instruct the survey to terminate.
        
        * @returns {Boolean}
        */
        this.terminateOnAnswer = function (answerIDs) {
            var answerIDsArray,
                thisAnswerID,
                i,
                terminate = false;

            answerIDsArray = answerIDs.split(',');

            for (i = 0; i < answerIDsArray.length; i++) {
                thisAnswerID = answerIDsArray[i];
                if (parent.obj.getAnswerObjectByID(thisAnswerID).goto_endscreen === 'true') {
                    terminate = true;
                    break;
                }
            }
            return terminate;
        };

        /**
        * Determine if any of the answers have link URL.
        
        * @param {string} answerIDs
        * @returns {Boolean}
        */
        this.urlOnAnswer = function (answerIDs) {
            var answerIDsArray,
                thisAnswerID,
                i,
                isUrl = false;

            answerIDsArray = answerIDs.split(',');

            for (i = 0; i < answerIDsArray.length; i++) {
                thisAnswerID = answerIDsArray[i];
                if (parent.obj.getAnswerObjectByID(thisAnswerID).url !== '') {
                    isUrl = true;
                    break;
                }
            }
            return isUrl;
        };

        /**
        * Determine if this user has already registered.
        
        * @returns {Boolean}
        */
        this.userHasNotRegistered = function () {
            var userCookies = data.getPersistentSurveyData('DSM_COOKIES'),
                userHasNotRegistered = true;

            if (userCookies === undefined) {
                userHasNotRegistered = true;
            } else if (userCookies.respondentEmail !== '') {
                userHasNotRegistered = false;
            }
            return userHasNotRegistered;
        };

        /**
        * Determine if the current survey screen is a creative screen.
        
        * @returns {Boolean}
        */
        this.isCreativeScreen = function () {
            var surveyScreens = data.getPersistentSurveyData('DSM_SURVEY_SCREENS');
            return surveyScreens[0].indexOf('data-template="creative"') > -1 ? true : false;
        };

        /**
        * Determine if the survey screen is the last one.
        
        * @returns {Boolean}
        */
        this.isLastScreen = function () {
            var surveyScreens = data.getPersistentSurveyData('DSM_SURVEY_SCREENS');
            return surveyScreens.length === 1 ? true : false;
        };

        /**
        * Determine if tracking pixels have been associated.
        
        * @returns {Mixed} An array of tracking pixel values, or false.
        */
        this.hasTrackingPixels = function () {
            var surveyData = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY;
            return surveyData.pixels || false;
        };

        /**
        * Determine if the current survey screen is a End screen.
        
        * @returns {Boolean}
        */
        this.isEndScreen = function () {
            var surveyScreens = data.getPersistentSurveyData('DSM_SURVEY_SCREENS');
            return surveyScreens[0].indexOf('data-name="endScreen"') > -1 ? true : false;
        };
        /**
        * Determine if the end screens are managed by creative group.
        
        * @returns {Boolean}
        */
        this.manageEndscreensByGroup = function () {
            var manageByGroup = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.endscreens.manage_by_group;
            return manageByGroup === 'true' || false;
        };

        /**
        * Determine if a tag parameter is defined.
        
        * @returns {Boolean}
        */
        this.scriptParameterIsDefined = function (parameterName) {
            var surveyNodeID = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.tagId,
                tagParameters = dom.read.getTagParameters(document.getElementById(surveyNodeID));
            tagParameters = parent.obj.urlStringToObject(tagParameters);

            return tagParameters[parameterName] ? true : false;
        };

        /**
        * Checks if the Respondent is OPT-OUT.
        
        * @returns {Boolean}
        */
        this.isSurveyOPTOUT = function () {
            var dsmCookie = data.getPersistentSurveyData('DSM_COOKIES');
            return dsmCookie.respondentID === 'OPT-OUT' ? true : false;
        };

        /**
        * Checks if the survey is active.
        
        * @returns {Boolean}
        */
        this.surveyIsLive = function () {
            var surveyData = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.project_info;
            return surveyData.is_live === 'true' ? true : false;
        };

        /**
         * Checks if Cookie count (Global/Project invite cookie) and returns true if it exceeds.
         * 
         * @param {object} dsmCookie Available Cookies
         * @returns {Boolean}
         */
        this.checkSurveyMaxInvite = function () {
            var dsmCookie = data.getPersistentSurveyData('DSM_COOKIES'),
                globalCookieLength = dsmCookie.dmisrvy_global_invite.split(',').length,
                projectCookieLength = dsmCookie.dmisrvy_project_invite.split(',').length;

            return globalCookieLength >= 100 || projectCookieLength >= 100 ? true : false;
        };
        /**
         * Checks if given script file is loaded into DOM or not.
         * 
         * @param {string} scriptPath Script path
         * @param {method} timer
         * @returns {Boolean} Returns boolean value 
         */
        this.isRequestedJsLoaded = function (scriptPath, timer) {
            var len = $('script[src="' + scriptPath + '"]').length;
            if (len > 0) {
                clearInterval(timer);
            }
            return len > 0 ? true : false;
        };

        this.IsGroupNameAvailable = function (groupName) {
            var endScreens = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.endscreens,
                groupNameArr = Object.keys(endScreens),
                i;
            for (i = 0; i < groupNameArr.length; i++) {
                if (groupNameArr[i] === groupName) {
                    return true;
                }
            }

            return false;
        };
    };
});