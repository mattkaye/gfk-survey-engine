/**
 @fileOverview Low level utility methods for string based values.

 @module Utility/String
 @author Matt Kaye
 @requires jQuery
 @requires uaParser
 */
define(function (require) {
    'use strict';
    var UaParser = require('uaParser'),
        $ = require('jquery');

    return function () {
        var parser = new UaParser();

        /**
        * Remove a parameter and it's value from a URL string.
        
        * @param {String} key Parameter key to use.
        * @param {String} sourceURL A well formatted URL.
        * @returns {String} A new URL string with the matched parameter and it's value removed.
        */
        this.removeParameterFromURLString = function (key, sourceURL) {
            var i,
                param,
                paramsArray = [];
            paramsArray = sourceURL.split('&');
            for (i = paramsArray.length - 1; i >= 0; i -= 1) {
                param = paramsArray[i].split('=')[0];
                if (param === key) {
                    paramsArray.splice(i, 1);
                }
            }
            return paramsArray.join('&');
        };


        /**
        * Generate a random strong of alphanumeric characters at a given length.
        
        * @param {Integer} length Length of the random string.
        * @returns {String} result A randomly generated string of '<i>length</i>' characters.
        */
        this.generateRandomString = function (length) {
            var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
                result = '',
                i;

            for (i = length; i > 0; --i) {
                result += chars[Math.round(Math.random() * (chars.length - 1))];
            }
            return result;
        };


        /**
         * Get the user agent string.
         * @returns {String} See <a href="http://faisalman.github.io/ua-parser-js/" target="_blank">documentation</a>
         */
        this.getUserAgentInformation = function () {
            return parser.getResult();
        };

        /**
        * Formats the User Agent string so it's acceptable to the API.
        
        * @param {String} key Parameter key to use.
        * @returns {String} uaFormattedString A User Agent string with proper formatting for the API.
        */
        this.formatUserAgentInformationForAPI = function () {
            var uaObject,
                uaFormattedString;

            uaObject = this.getUserAgentInformation();
            uaFormattedString = uaObject.browser.name + '_' + uaObject.browser.version + '_' + (uaObject.device.model || 'Desktop') + '_' + uaObject.os.version;

            return uaFormattedString;
        };

        /**
        * Get the value portion of a key|value pair in a URL.
        
        * @param {String} parameterName The key to use.
        * @param {String} source The string to search though.
        * @returns {String} Value of matching parmaterName. Empty if no matching key is found.
        */
        this.getParameterValueByName = function (parameterName, source) {
            var regex,
                results;

            parameterName = parameterName.replace(/[\[]/, "").replace(/[\]]/, "");
            regex = new RegExp("[\\?&]" + parameterName + "=([^&#]*)");
            results = regex.exec(source);
            return results === null ? "" : this.decodeString(results[1].replace(/\+/g, " "));
        };

        /**
        * Removes the Dimestore macros from a given string.
        
        * @param {String} string The string to search.
        * @returns {String} string Returns the same argument passed in with the start and end macros removed.
        */
        this.stripMacros = function (string) {
            if (typeof string === 'string') {
                string = string.replace(/DMIMACROS/g, '').replace(/DMIMACROE/g, '');
            }
            return string;
        };

        /**
        * <i style="color:red;">NOTE: INCORRECT METHOD FUNCTION. MUST BE FIXED TO READ QUESITON SPECIFIC INTEGRATION BASE URL.</i>
        
        * @param {String} key Parameter key to use.
        * @param {String} sourceURL A well formatted URL.
        * @returns {String} A new URL string with the matched parameter and it's value removed.
        */
        this.getIntegrationBaseURL = function (questions) {
            var i,
                x,
                externalTrackingUrl;

            externalTrackingUrl = false;

            for (i = 0; i < questions.length; i++) {
                if (externalTrackingUrl) {
                    break;
                }
                if (questions[i].answers) {
                    for (x = 0; x < questions[i].answers.length; x++) {
                        externalTrackingUrl = questions[i].answers[x].external_tracking;
                        if (externalTrackingUrl) {
                            break;
                        }
                    }
                }
            }
            return externalTrackingUrl;
        };

        /**
        * Join together one or more duplicate key|value pairs in a URL.
        
        * @param {String} urlString String to look through.
        * @param {String=} separator What character will separte the values. Default is comma.
        * @returns {String} urlString A new URL string with all duplicate key values joined under a single key separated by '<i>separator</i>'.
        */
        this.joinDuplicateURLParameterValues = function (urlString, separator) {
            var i,
                prop,
                parameterKey,
                parameterValue,
                parametersArray,
                parametersObject;

            separator = separator || ',';
            parametersObject = {};
            parametersArray = urlString.split('&');

            for (i = 0; i < parametersArray.length; i++) {
                parameterKey = parametersArray[i].split('=')[0];
                parameterValue = parametersArray[i].split('=')[1];

                if (!parametersObject[parameterKey]) {
                    parametersObject[parameterKey] = [];
                }
                parametersObject[parameterKey].push(parameterValue);
            }

            for (prop in parametersObject) {
                if (parametersObject.hasOwnProperty(prop)) {
                    parametersObject[prop] = parametersObject[prop].join(separator);
                }
            }

            urlString = $.param(parametersObject);
            urlString = this.decodeString(urlString);

            return urlString;
        };

        /**
        * Decodes a string value that may intentionally be useing reserved URL characters.
        
        * @param {String} string String to decode.
        * @returns {String} string The decoded string.
        */
        this.decodeString = function (string) {
            try {
                string = decodeURIComponent(string);
            } catch (e) {
                string = unescape(string);
            }
            return string;
        };

        /**
        * Encodes a string value that may intentionally be useing reserved URL characters.
        
        * @param {String} string String to encode.
        * @returns {String} string The encoded string.
        */
        this.encodeString = function (string) {
            try {
                string = encodeURIComponent(string);
            } catch (e) {
                string = escape(string);
            }
            return string;
        };

        /**
        * Formats open ended answer text for the API to process.
        
        * @param {String} text String to format.
        * @returns {String} string The formatted string.
        */
        this.formatOpenEndedAnswerText = function (text) {
            text = text.replace(/\+/g, " ");
            text = text.replace(/%0D%0A/g, '[newline]');
            return decodeURIComponent(text);
        };
    };
});