/**
 @fileOverview Low level utility methods for Objects.

 @module Utility/Object
 @author Matt Kaye
 */
define(function (require) {
    'use strict';

    var DataModule = require('../data/Data'),
        DomModule = require('../dom/Dom'),
        cssJSON = require('cssjson'),
        $ = require('jquery');

    return function (parent) {
        var data = new DataModule(),
            dom = DomModule;

        /**
        * Converts a well formatted URL parameter string to an Object.
        
        * @param {String} urlString A URL string of values to convert.
        * @returns {Object} An object with key/value pairs to represent the string values.
        */
        this.urlStringToObject = function (urlString) {
            var temp,
                newObject,
                tempString,
                i;

            temp = urlString.split("&");
            newObject = {};
            for (i = 0; i < temp.length; i++) {
                tempString = temp[i];
                newObject[tempString.split("=")[0]] = tempString.split("=")[1];
            }
            return newObject;
        };

        /**
        * Given an answerID, get the entire answer object.
        
        * @param {String} answerID An answer ID.
        * @returns {Mixed} The full answer object. False if answer ID is not found, or question type is open text or demographic.
        */
        this.getAnswerObjectByID = function (answerID) {
            var questionData = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.questions,
                i,
                x,
                answerObject = false;

            for (i = 0; i < questionData.length; i++) {
                if (questionData[i].answers) {
                    for (x = 0; x < questionData[i].answers.length; x++) {
                        if (questionData[i].answers[x].id === answerID) {
                            answerObject = questionData[i].answers[x];
                            break;
                        }
                    }
                }
            }
            return answerObject;
        };

        /**
        * Converts a well formatted JSON object to CSS rules.
        
        * @param {Object} JSONData CSS styles represensted in JSON format.
        * @returns {String} CSS rules.
        */
        this.JSONToCSS = function (JSONData) {
            return cssJSON.toCSS(JSONData);
        };
        /**
         * Checks for project data cookie and sets and returns the peCount to the requested payload. 
         
         * @param {Object} requestPayload The data payload
         * @param {Boolean} assignExposure True or False
         * @returns {Object} The altered request Payload
         */
        this.getProjectExposureCount = function (requestPayload, assignExposure) {
            var projectsCookie = requestPayload.dmisrvy_project_data,
                exposureCount = '',
                arrProjects,
                isProjectIdContains,
                projectId = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.pid,
                arrData,
                objProjectData;

            if (projectsCookie !== undefined) { //cookie not present
                arrProjects = projectsCookie.split(",");

                $.each(arrProjects, function (index, value) {
                    if (value.split(":")[0] === projectId.toString()) {
                        isProjectIdContains = index;
                    }
                });

                if (isProjectIdContains !== -1) { //for this project data not present.
                    objProjectData = arrProjects[isProjectIdContains];

                    if (objProjectData !== undefined) {
                        arrData = (objProjectData.toString()).split(":");

                        if (arrData[1] !== "" && arrData[1] !== undefined) {
                            exposureCount = arrData[1];

                        }
                    }
                }
            }
            requestPayload.peCount = exposureCount === '' ? '' : parseInt(exposureCount, 10);
            if (assignExposure) {
                if (0 === exposureCount) {
                    requestPayload.exposure = "control";
                } else if (0 < exposureCount) {
                    requestPayload.exposure = "exposed";
                }
            }
            return requestPayload;

        };

        /**
        * Get the survey's width and height.
        
        * @returns {Object} The width and height of this survey.
        */
        this.getSurveyDimensions = function () {
            var surveyData = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.project_info;

            if (parent.bool.isMultiSizeSurvey()) {
                surveyData = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS;
                surveyData.width = surveyData.bgWidth;
                surveyData.height = surveyData.bgHeight;
            }

            return {
                width: parseInt(surveyData.width, 10),
                height: parseInt(surveyData.height, 10)
            };
        };

        /**
        * Get the creative based on ID passed.
        
        * @param {String} creativeID The creative ID.
        * @returns {Object} The creative object.
        */
        this.getCreativeByID = function (creativeID) {
            var allCreatives = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.creatives,
                foundCreative,
                i;

            for (i = 0; i < allCreatives.length; i++) {
                if (allCreatives[i].id === creativeID) {
                    foundCreative = allCreatives[i];
                }
            }
            data.setPersistentSurveyData('DSM_SURVEY_CHOSEN_CREATIVE', foundCreative);
            return foundCreative;
        };

        /**
        * Get a randomly selected creative that conforms to the survey size.
        
        * @param {Array} creativeIDs An array of creative IDs to choose from.
        * @returns {Object} A random creative object.
        */
        this.getRandomCreative = function (creativeIDs) {
            var allCreatives = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.creatives,
                surveyNodeID = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.tagId,
                surveyWidth = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.bgWidth,
                surveyHeight = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.bgHeight,
                randomCreative = false,
                scriptTagParams,
                clickMacroURL,
                usableCreatives = [],
                i;

            for (i = 0; i < allCreatives.length; i++) {
                if ($.inArray(allCreatives[i].id, creativeIDs) === -1) {
                    continue;
                }

                //Multisize creatives can have various dimensions. We have to check for dimensions
                if (parent.bool.isMultiSizeSurvey()) {
                    if (parseInt(allCreatives[i].creative_width, 10) === surveyWidth && parseInt(allCreatives[i].creative_height, 10) === surveyHeight) {
                        usableCreatives.push(allCreatives[i]);
                    }
                } else {
                    usableCreatives.push(allCreatives[i]);
                }
            }
            if (usableCreatives.length) {
                randomCreative = usableCreatives[Math.floor(Math.random() * usableCreatives.length)];
            } else {
                randomCreative = allCreatives[0];
            }

            //If click macro is defined, use that before the click URL from the creative.
            if (parent.bool.scriptParameterIsDefined('clickUrl')) {
                //Get all tag params
                scriptTagParams = dom.read.getTagParameters(document.getElementById(surveyNodeID));

                //Search for the clickUrl parameter value
                clickMacroURL = parent.string.getParameterValueByName('clickUrl', scriptTagParams);

                //Strip macro from value
                clickMacroURL = parent.string.stripMacros(clickMacroURL);

                //Assign new click URL
                randomCreative.click_url = clickMacroURL + randomCreative.click_url;
            }

            data.setPersistentSurveyData('DSM_SURVEY_CHOSEN_CREATIVE', randomCreative);
            return randomCreative;
        };

        /**
        * Get all creative IDs based on a list of group names.
        
        * @param {Array} creativeGroupNames An array of one or more group names to choose from.
        * @returns {Array} An array of creative IDs.
        */
        this.getCreativeIDsByGroup = function (creativeGroupNames) {
            var allCreatives = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.creatives,
                creativeIDs = [],
                i;

            for (i = 0; i < allCreatives.length; i++) {
                if ($.inArray(allCreatives[i].group_name, creativeGroupNames) > -1) {
                    creativeIDs.push(allCreatives[i].id);
                }
            }
            return creativeIDs;
        };

        /**
        * Get meta data for the endscreen we're going to use.
        
        * @param {String} groupName Name of the creative group.
        * @returns {Object}
        */
        this.getEndscreenMeta = function (groupName) {
            var isMultiSize = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.multi_size,
                isGroupNameAvailable = parent.bool.IsGroupNameAvailable(groupName),
                surveyWidth = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.bgWidth,
                groupEndScreen,
                groupNameArray,
                endscreen = {},
                randomEndScreen = {},
                endScreenObj,
                i,
                controlGroup;

            if (isMultiSize === 'true') {

                if (isGroupNameAvailable) {
                    groupEndScreen = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.endscreens[groupName];
                    for (i = 0; i < groupEndScreen.length; i++) {
                        if (surveyWidth.toString() === groupEndScreen[i].creative_width) {
                            endscreen = groupEndScreen[i];
                            return endscreen;
                        }
                    }
                }
                return this.getMatchingDimensionEndscreen(groupName);

            }
            if (isMultiSize === 'false' && groupName === 'exposed') {
                endScreenObj = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.endscreens;
                controlGroup = endScreenObj.control;
                if (controlGroup) {
                    delete endScreenObj.control;
                    delete endScreenObj.manage_by_group;

                    groupNameArray = Object.keys(endScreenObj);
                    endscreen = endScreenObj[groupNameArray[groupNameArray.length * Math.random() < 0]];
                    randomEndScreen.endscreen = [];
                    randomEndScreen.endscreen.push(endscreen);
                    return endscreen.hasOwnProperty('property') || !endscreen.hasOwnProperty('type') ? endscreen : randomEndScreen;

                }
            }
            if (isMultiSize === 'false' && groupName === 'control') {
                return data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.endscreens.control;
            }

            return data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.endscreens[groupName];
        };

        /*
         * Traverse end screen object and returns randomly returns endscreen 
         * of matching dimension of belonging group name.
         *  
         * @param {string} groupName
         * @returns {Array} Returns endscreen of provided group name with matching dimention
         */
        this.getMatchingDimensionEndscreen = function (groupName) {
            var endScreens = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.endscreens,
                matchingEndScreenArr = [],
                groupNameArr = Object.keys(endScreens),
                surveyWidth = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.bgWidth,
                validGroupName,
                i,
                j;

            for (i = 0; i < groupNameArr.length; i++) {
                validGroupName = groupNameArr[i] !== 'manage_by_group' && groupName !== 'exposed' ? true : false;
                if (validGroupName) {
                    for (j = 0; j < endScreens[groupNameArr[i]].length; j++) {
                        if (endScreens[groupNameArr[i]][j].creative_width === surveyWidth.toString()) {
                            matchingEndScreenArr.push(endScreens[groupNameArr[i]][j]);
                        }
                    }
                } else {
                    if (groupNameArr[i] !== 'control') {
                        for (j = 0; j < endScreens[groupNameArr[i]].length; j++) {
                            if (endScreens[groupNameArr[i]][j].creative_width === surveyWidth.toString()) {
                                matchingEndScreenArr.push(endScreens[groupNameArr[i]][j]);
                            }
                        }
                    }
                }
            }

            return matchingEndScreenArr[Math.floor(Math.random() * matchingEndScreenArr.length)];
        };
        /**
         * Get random meta data for the endscreen if dont show creative is configured in tag.
         * @returns {Object}
         */
        this.getRandomEndscreenMeta = function () {
            var endScreenMeta,
                endScreensObject = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.endscreens,
                manageByGroupsArr = Object.keys(endScreensObject),
                surveyWidth = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.bgWidth,
                isMultiSize = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.multi_size,
                randomGroup,
                endScreenWidth,
                endscreen = {},
                i,
                j,
                k;

            for (i = 0; i < manageByGroupsArr.length; i++) {
                if (manageByGroupsArr[i] === 'manage_by_group') {
                    manageByGroupsArr.splice(i, 1);
                    i =  i > 0 ? i-- : 0;
                }
                if (isMultiSize === 'true' && endScreensObject[manageByGroupsArr[i]] !== 'true') {
                    endScreenWidth = endScreensObject[manageByGroupsArr[i]][0].creative_width;
                } else {
                    endScreenWidth = endScreensObject[manageByGroupsArr[i]] !== 'true' ? endScreensObject[manageByGroupsArr[i]].endscreen[0].creative_width : '';
                }

            }
            randomGroup = manageByGroupsArr[Math.floor(Math.random() * manageByGroupsArr.length)];

            endScreenMeta = endScreensObject[randomGroup];

            if (isMultiSize === 'true') {
                for (j = 0; j < endScreenMeta.length; j++) {
                    if (endScreenMeta[j].hasOwnProperty('endscreen')) {
                        for (k = 0; k < endScreenMeta[j].endscreen.length; k++) {
                            endScreenWidth = parseInt(endScreenMeta[j].endscreen[k].creative_width, 10);
                            if (endScreenWidth === surveyWidth) {
                                endscreen.endscreen = [];
                                endscreen.endscreen.push(endScreenMeta[j].endscreen[k]);
                                return endscreen;
                            }
                        }
                    }
                }
            } else {
                return endScreenMeta;
            }

        };

        this.getEndscreenByProperty = function (endScreenMeta, selectedCreativeGroupName) {
            var endScreenObject,
                allCreatives = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.creatives,
                groupName,
                endScreenObjLength,
                endScreens,
                surveyWidth = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.bgWidth,
                i,
                j,
                isMultiSize = parent.bool.isMultiSizeSurvey();

            if (endScreenMeta === undefined) {
                endScreenObject = {
                    noEndScreen: true
                };
                return endScreenObject;
            }

            if (endScreenMeta.endscreen) {
                endScreenObject = endScreenMeta.endscreen[0];
                return endScreenObject;
            }

            switch (endScreenMeta.property) {
            case '1': //No End Screen
                endScreenObject = {
                    noEndScreen: true
                };
                break;

            case '2': //Same As Creative
                if (data.getPersistentSurveyData('DSM_SURVEY_CHOSEN_CREATIVE')) {
                    endScreenObject = data.getPersistentSurveyData('DSM_SURVEY_CHOSEN_CREATIVE');
                } else {
                    if (this.getCreativeByID(endScreenMeta.id)) {
                        endScreenObject = this.getCreativeByID(endScreenMeta.id);
                    } else {
                        groupName = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.exposure;
                        for (i = 0; i < allCreatives.length; i++) {
                            isMultiSize = isMultiSize ? true : false;
                            if ((allCreatives[i].group_name === groupName && isMultiSize)
                                    || (allCreatives[i].group_name === groupName && allCreatives[i].group_name === 'control')) {

                                if (isMultiSize && surveyWidth.toString() === allCreatives[i].creative_width) {
                                    endScreenObject = allCreatives[i];
                                }
                                if (!isMultiSize) {
                                    endScreenObject = allCreatives[i];
                                }

                                break;
                            }
                        }
                        if (!endScreenObject) {
                            endScreenObject = this.getRamdomCreativeForEndScreen(allCreatives);
                        }
                    }
                }
                break;
            case '3': //Same as Default
                endScreenObject = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.endscreens.backup_for_all_groups;
                break;
            case '4': //Upload End Screen
                selectedCreativeGroupName = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.endscreens[selectedCreativeGroupName] ? selectedCreativeGroupName : false;

                if (selectedCreativeGroupName) {
                    endScreenObjLength = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.endscreens[selectedCreativeGroupName].length;
                    endScreens = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.endscreens[selectedCreativeGroupName][endScreenObjLength - 1].endscreen;

                    if (endScreens && endScreens.length > 0) {
                        for (j = 0; j < endScreens.length; j++) {
                            if (surveyWidth.toString() === endScreens[j].creative_width) {
                                endScreenObject = endScreens[j];
                                break;
                            }
                        }
                    } else {
                        endScreenObject = this.getAllExposedEndScreens();
                    }
                } else {
                    endScreenObject = this.getAllExposedEndScreens();
                }

                break;
            default:
                if (data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.endscreens.backup_for_all_groups) {
                    endScreenObject = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.endscreens.backup_for_all_groups;
                } else {
                    endScreenObject = {
                        noEndScreen: true
                    };
                }
                break;
            }
            return endScreenObject;
        };
        this.getAllExposedEndScreens = function () {
            var endScreenObject = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.endscreens,
                i,
                j,
                groupNames = Object.keys(endScreenObject),
                endScreen = [],
                endScreenLen,
                endScreenWidth,
                surveyWidth = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.bgWidth;

            for (i = 0; i < groupNames.length; i++) {
                if (groupNames[i] !== 'control' && groupNames[i] !== 'manage_by_group') {
                    endScreenLen = endScreenObject[groupNames[i]].length;
                    for (j = 0; j < endScreenObject[groupNames[i]][endScreenLen - 1].endscreen.length; j++) {
                        endScreenWidth = endScreenObject[groupNames[i]][endScreenLen - 1].endscreen[j].creative_width;
                        if (surveyWidth.toString() === endScreenWidth) {
                            endScreen.push(endScreenObject[groupNames[i]][endScreenLen - 1].endscreen[j]);
                        }
                    }
                }
            }
            return endScreen[Math.floor(Math.random() * endScreen.length)];
        };
        this.getRamdomCreativeForEndScreen = function (allCreatives) {
            var surveyWidth = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.bgWidth,
                i,
                exposure = data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.exposure,
                creativeArr = [];
            if (exposure === 'exposed') {
                for (i = 0; i < allCreatives.length; i++) {
                    if (allCreatives[i].group_name === 'control') {
                        allCreatives.splice(i, 1);
                    }

                }
            }
            if (parent.bool.isMultiSizeSurvey()) {
                for (i = 0; i < allCreatives.length; i++) {
                    if (allCreatives[i].creative_width === surveyWidth.toString()) {
                        creativeArr.push(allCreatives[i]);
                    }
                }
            } else {
                for (i = 0; i < allCreatives.length; i++) {
                    creativeArr.push(allCreatives[i]);
                }

            }

            return creativeArr[Math.floor(Math.random() * creativeArr.length)];
        };
        /**
         * Finds out the specific Question from its ID
         * @param {String} questionID
         * @returns {Object}
         */
        this.getQuestionObjectById = function (questionID) {
            var questionObject,
                questionnaireData = data.getPersistentSurveyData('DSM_SURVEY_DATA').SURVEY.questions,
                q;
            questionID = questionID.toString();
            for (q = 0; q < questionnaireData.length; q++) {
                if (questionID === questionnaireData[q].id) {
                    questionObject = questionnaireData[q];
                    break;
                }
            }
            return questionObject;
        };
        /**
         * Checks for link and terminate is configured on answer.
         *
         * @param {object} questionData
         * @returns {object} termincateObj Returns boject of Answer click url and terminate value.
         */
        this.checkUrlAndTerminate = function (questionData) {
            var terminateObj = {},
                isUrl = false,
                isTerminate;

            if (parent.bool.urlOnAnswer(questionData.answerID)) {
                isUrl = true;
                if (parent.bool.terminateOnAnswer(questionData.answerID)) {
                    isTerminate = true;
                } else {
                    isTerminate = false;
                }
            }
            terminateObj.urlWithTerminate = isUrl === true && isTerminate === true;
            terminateObj.urlWithNoTerminate = isUrl === true && isTerminate !== undefined && isTerminate === false;
            return terminateObj;
        };

    };
});