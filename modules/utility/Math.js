/**
 @fileOverview Low level utility methods for math based values.

 @module Utility/Math
 @author Matt Kaye
 */
define([], function () {
    'use strict';

    return function () {
        /**
        * Survey timer for each survey question.
        
        * @public
        * @param {Object=} options Consists of two callback methods:<br>
        * <b>onUpdateStatus()</b> - Fired each second<br>
        * <b>onCounterEnd()</b> - Fired when the timer reaches zero. 
        
        * @returns {String} Time left on the clock in mintues:seconds format.
        */
        this.surveyTimer = function (options) {
            var timer,
                instance = this,
                minutes,
                secondsMinusMinutes,
                remainingTime,
                seconds = options.seconds || 30,
                updateStatus = options.onUpdateStatus || function () {
                    return undefined;
                },
                counterEnd = options.onCounterEnd || function () {
                    return undefined;
                };

            function zeroPad(n) {
                return (n < 10) ? ("0" + n) : n;
            }

            function decrementCounter() {
                minutes = Math.floor(seconds / 60);
                secondsMinusMinutes = seconds - minutes * 60;
                remainingTime = minutes + ':' + zeroPad(secondsMinusMinutes);

                updateStatus(remainingTime);
                if (seconds === 0) {
                    counterEnd();
                    instance.stop();
                }
                seconds -= 1;
            }

            this.start = function () {
                clearInterval(timer);
                timer = 0;
                seconds = options.seconds;
                timer = setInterval(decrementCounter, 1000);
                return timer;
            };

            this.stop = function () {
                clearInterval(timer);
            };
            return this;
        };
    };
});