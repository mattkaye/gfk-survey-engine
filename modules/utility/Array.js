/**
 @fileOverview Low level utility methods for arrays values.

 @module Utility/Array
 @author Matt Kaye
 @requires jQuery
 */
define(function (require) {
    'use strict';
    var $ = require('jquery');

    return function () {
        /**
        * Randomize the answers to a give question. Takes into account questions that have been marked as 'anchored' in the Dimestore admin panel.
        
        * @param {Array} array The array of answers to shuffle.
        * @returns {Array}
        */
        this.shuffleQuestionAnswers = function (array) {
            var anchors = [],
                arrIndexes = [],
                tempArray,
                randomNumber,
                i;

            for (i = 0; i < array.length; i++) {
                arrIndexes.push(i);
                if (array[i].anchor === 'true') {
                    anchors.push(i);
                }
            }

            if (anchors.length) {
                for (i = 0; i < anchors.length; i++) {
                    delete arrIndexes[anchors[i]];
                }

                arrIndexes = $.grep(arrIndexes, function (n) {
                    return n;
                });

                if (!arrIndexes.length) {
                    return array;
                }
            }

            for (i = 0; i < array.length; i++) {
                if (array[i].anchor !== 'true') {
                    randomNumber = arrIndexes[Math.floor(Math.random() * arrIndexes.length)];
                    tempArray = array[i];
                    array[i] = array[randomNumber];
                    array[randomNumber] = tempArray;
                }
            }
            return array;
        };

        /**
        * Organizes the final template markup screens in the order they will appear in the survey.
        
        * @param {Array} correctOrder An array of template names in the correct order.
        * @param {Array} surveyTemplates An array of all the survey screens markup.
        * @returns {Array} All the markup for the survey screens in the correct order from start to finish.
        */
        this.ensureCorrectScreenOrder = function (correctOrder, surveyTemplates) {
            var fixedTemplateOrder = [],
                i,
                x;
            for (i = 0; i < correctOrder.length; i++) {
                for (x = 0; x < surveyTemplates.length; x++) {
                    if (surveyTemplates[x].indexOf('data-template="' + correctOrder[i] + '"') > -1) {
                        fixedTemplateOrder.push(surveyTemplates[x]);
                        break;
                    }
                }
            }
            return fixedTemplateOrder;
        };
    };
});