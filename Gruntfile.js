module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        bowerRequirejs: {
            target: {
                rjsConfig: 'dimestoreTag.js'
            }
        },
        jsdoc: {
            dist: {
                src: [
                    'README.md',
                    'modules/**/*.js'
                ],
                options: {
                    destination: 'documentation',
                    template: 'jsdocTemplate',
                    configure: 'jsdoc.conf.json',
                    tutorials: 'documentation/tutorials'
                }
            }
        },
        jslint: {
            client: {
                src: [
                    'modules/**/*.js',
                    'dimestoreTag.js',
                    '!modules/lib/*.js'
                ],

                directives: {
                    browser: true,
                    plusplus: true,
                    indent: 4,
                    maxerr: 100,
                    devel: true,
                    'continue': true,
                    nomen: true,
                    regexp: true,
                    predef: [
                        'jQuery',
                        'require',
                        'define',
                        'window',
                        'console',
                        'escape',
                        'unescape',
                        '$'
                    ]
                },
            }
        },
        watch: {
            scripts: {
                files: [
                    'modules/**/*.js',
                    'dimestoreTag.js'
                ],
                tasks: ['jslint', 'jsdoc'],
                options: {
                    debounceDelay: 300,
                    livereload: true
                },
            },
        },
        requirejs: {
            compile: {
                options: {
                    baseUrl: "",
                    mainConfigFile: "dimestoreTag.js",
                    name: "bowerComponents/almond/almond",
                    include: ['dimestoreTag'],
                    out: "dist/html5/dimestoreTag.js",
                    preserveLicenseComments: false,
                }
            }
        },
        strip: {
            main: {
                src: 'dist/html5/dimestoreTag.js',
                dest: 'dist/html5/dimestoreTag.js'
            }
        },
        copy: {
            assets: {
                src: 'assets/**/*',
                dest: 'dist/',
                expand: true
            },
            templates: {
                src: 'templates/**/*',
                dest: 'dist/',
                expand: true
            }
        },
        clean: {
            build: {
                src: [ 'dist' ]
            }
        }
    });
    grunt.loadNpmTasks('grunt-bower-requirejs');
    grunt.loadNpmTasks('grunt-jslint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-jsdoc');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-strip');
    grunt.loadNpmTasks('grunt-contrib-clean')
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.registerTask('default', ['clean', 'bowerRequirejs', 'jsdoc', 'requirejs', 'strip', 'copy']);
    grunt.registerTask('dist', ['requirejs']);
};