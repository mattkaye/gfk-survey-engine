(function () {
    'use strict';
    var head,
        script,
        isRequireJSLoaded;
    head = document.getElementsByTagName('head')[0];
    script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = '//cdnjs.cloudflare.com/ajax/libs/require.js/2.1.17/require.min.js';
    head.appendChild(script);

    function surveyInit() {
        require.config({
            baseUrl: '//localhost/frontend-framework/',
            waitSeconds: 60,
            paths: {
                'jquery': 'bowerComponents/jquery/dist/jquery',
                'mustache': 'bowerComponents/mustache/mustache',
                'storage': 'bowerComponents/store-js/store',
                'uaParser': 'bowerComponents/ua-parser-js/src/ua-parser',
                'store-js': 'bowerComponents/store-js/store',
                'ua-parser-js': 'bowerComponents/ua-parser-js/src/ua-parser',
                'cssjson': 'node_modules/cssjson/cssjson',
                'almond': 'bowerComponents/almond/almond',
                'lodash': 'bowerComponents/lodash/lodash'
            },
            map: {
                '*': {
                    jquery: 'modules/lib/jqueryPrivate'
                },
                'modules/lib/jqueryPrivate': {
                    jquery: 'jquery'
                }
            }
        });

        require(
            [
                './modules/utility/Utility',
                './modules/environment/Environment',
                './modules/data/Data',
                './modules/dom/Dom',
                './modules/api/Api',
                './modules/cookie/Cookie',
                './modules/tags/Tags',
                './modules/views/Views',
                './modules/decisioning/Decisioning',
                'storage',
                'jquery'
            ],
            function (
                UtilityModule,
                EnvironmentModule,
                DataModule,
                DomModule,
                ApiModule,
                CookieModule,
                TagsModule,
                ViewsModule,
                DecisioningModule,
                storage,
                $
            ) {
                if (!storage.enabled) {
                    console.error('Local storage is not supported by your browser. Please disable "Private Mode", or upgrade to a modern browser.');
                    return;
                }

                var surveyTag,
                    tagParameters,
                    surveyData,
                    tagParametersObject,
                    surveyScreenOrder,
                    baseAssetURL,
                    tagType,
                    loadedSurveyScreens = [],
                    survey;

                survey = {
                    utility: UtilityModule,
                    environment: new EnvironmentModule(),
                    data: new DataModule(),
                    dom: DomModule,
                    api: new ApiModule(),
                    cookie: new CookieModule(),
                    tags: new TagsModule(),
                    views: new ViewsModule(),
                    decisioning: new DecisioningModule()
                };

                //Survey Tag Parameters
                surveyTag = survey.dom.read.findScriptTagByFileName('dimestoreTag');
                tagParameters = survey.dom.read.getTagParameters(surveyTag);
                tagParametersObject = survey.utility.obj.urlStringToObject(tagParameters);

                if (survey.data.getPersistentSurveyData('DSM_SURVEY_CHOSEN_CREATIVE')) {
                    survey.data.removePersistentSurveyData('DSM_SURVEY_CHOSEN_CREATIVE');
                }


                //Store JSON for tag, survey, and theme
                survey.data.getSurveyJSON(tagParametersObject.projectID, tagParametersObject.scriptID, function (data) {
                    var key,
                        commonAPIParameters,
                        eventSpecificAPIParameters,
                        mergedAPIParameters,
                        url,
                        apiPath = survey.environment.getEnvironmentPath().API,
                        isScriptLoaded,
                        checkScriptInterval;

                    /*
                    Merge the parameters from the actual JavaScript tag with the global object defined from the the call to:
                    http://stagingadmin2-dimestore.dmi.sensic.net/prod/tags/v3/12078/dsmTag43.js
                    */
                    for (key in tagParametersObject) {
                        if (tagParametersObject.hasOwnProperty(key)) {
                            data.JS_TAG_PARAMETERS[key] = tagParametersObject[key];
                            data.JS_TAG_PARAMETERS[key] = survey.utility.string.stripMacros(data.JS_TAG_PARAMETERS[key]);
                        }
                    }
                    data.JS_TAG_PARAMETERS.invitationValue = new Date().getTime();

                    survey.data.setPersistentSurveyData('DSM_SURVEY_DATA', data);
                    //Call the requested API method to capture the sensic cookies
                    commonAPIParameters = survey.api.helper.buildCommonApiParameters(data.JS_TAG_PARAMETERS);
                    eventSpecificAPIParameters = survey.api.helper.buildEventSpecificApiParameters('requested', data.JS_TAG_PARAMETERS);

                    mergedAPIParameters = $.extend({}, commonAPIParameters, eventSpecificAPIParameters);
                    if (data.JS_TAG_PARAMETERS.previewMode) {
                        apiPath = survey.environment.getEnvironmentPath().CDN + 'event/requested.js';
                        checkScriptInterval = setInterval(function () {
                            isScriptLoaded = survey.utility.bool.isRequestedJsLoaded(apiPath + '?' + $.param(mergedAPIParameters), checkScriptInterval);
                            if (isScriptLoaded === true) {
                                window.getQueryString(apiPath + '?' + $.param(mergedAPIParameters));
                            }
                        }, 1000);
                    }
                    url = apiPath + '?' + $.param(mergedAPIParameters);
                    survey.dom.create.writeScriptTagToHead(url, 'dsm_script_tag');
                });

                baseAssetURL = survey.environment.getEnvironmentPath().LOCAL_INSTALL_PATH || survey.environment.getEnvironmentPath().CDN;

                function getTemplateMarkup(templateObject) {
                    var templateURL,
                        templateName;

                    templateName = Object.keys(templateObject);
                    templateURL = baseAssetURL + templateObject[templateName];
                    survey.dom.create.writeScriptTagToHead(templateURL);
                }
                function allScreensLoaded(surveyTemplates) {
                    var renderedTemplates,
                        scriptTagNode,
                        surveyParentNode,
                        finalTemplateOrder = [];

                    finalTemplateOrder = survey.utility.array.ensureCorrectScreenOrder(surveyScreenOrder, surveyTemplates);

                    //Render all survey screens
                    renderedTemplates = survey.views.renderSurveyScreens(finalTemplateOrder, surveyData);

                    //Find script tag
                    scriptTagNode = document.getElementById(surveyData.JS_TAG_PARAMETERS.tagId);

                    //Load tag dependencies
                    survey.tags.helper.loadTagDependencies(tagType, function () {
                        //Write node to body that will be the parent DOM node
                        var surveyParentNodeLocation = scriptTagNode,
                            maxCharacterCount,
                            surveyDimensions,
                            completedProjects,
                            hasTakenSurvey,
                            surveyDimensionsClassName,
                            surveyAdData;

                        if (tagType.targetPlatform === 'mobile web') {
                            surveyParentNodeLocation = 'body';
                        }

                        $(surveyParentNodeLocation).before('<div id="' + scriptTagNode.id + '-frame" class="dsm-frame"></div>');

                        surveyParentNode = $('#' + scriptTagNode.id + '-frame');

                        if (tagType.targetPlatform === 'desktop') {
                            if (tagType.type === 'overlay') {
                                surveyAdData = survey.dom.read.findCreative(0, surveyData.JS_TAG_PARAMETERS);
                                window.surveyAdData = surveyAdData;
                                surveyParentNode = surveyAdData.adParentNode;
                            }
                            surveyDimensions = survey.utility.obj.getSurveyDimensions();
                            surveyDimensionsClassName = survey.tags.helper.getSurveyDimensionsClass(surveyDimensions.width, surveyDimensions.height);
                            $(surveyParentNode).addClass(surveyDimensionsClassName);
                        }

                        //Write the survey wrapper to the DOM
                        $(surveyParentNode).html(renderedTemplates[0]);

                        renderedTemplates.shift();

                        survey.data.setPersistentSurveyData('DSM_SURVEY_SCREENS', renderedTemplates);

                        //One survey complete per user

                        if (survey.utility.bool.oneSurveyPerUser()) {
                            completedProjects = survey.data.getPersistentSurveyData('DSM_COOKIES').dmisrvy_project_complete;
                            if (completedProjects) {
                                hasTakenSurvey = survey.utility.bool.hasCompletedSurvey(tagParametersObject.projectID, completedProjects.split(','));
                                if (hasTakenSurvey && tagType.type === 'in banner') {
                                    survey.dom.update.goToEndscreen();
                                } else {
                                    survey.dom.update.hideSurvey();
                                }
                                return;
                            }
                        }

                        if (survey.utility.bool.surveyWillLoop()) {
                            survey.data.setPersistentSurveyData('DSM_SURVEY_SCREENS_TEMPLATE', renderedTemplates);
                        }

                        //First survey screen

                        if (survey.utility.bool.isSurveyOPTOUT()) {
                            if (tagType.type === 'overlay') {
                                survey.dom.update.hideSurvey();
                            } else {
                                survey.dom.update.goToEndscreen();
                            }
                            return;
                        }

                        survey.dom.update.moveToNextScreen();

                        //Common to all tag types
                        survey.dom.listen.checkboxSelectionListener();
                        survey.dom.listen.nextQuestionListeners();
                        survey.dom.listen.registrationListener();

                        maxCharacterCount = survey.data.getOpenTextQuestionMaxCharacterCount(surveyData.SURVEY.questions);
                        survey.dom.listen.limitCharacterCount(maxCharacterCount);
                    });
                }
                function surveyNotEligible(surveyData) {
                    var commonApiParam,
                        eventSpecificAPIParameters,
                        mergedAPIParameters,
                        dsmCookie = survey.data.getPersistentSurveyData('DSM_COOKIES'),
                        url,
                        projectExposureCount;

                    if (dsmCookie.peCount === -1) {
                        surveyData.JS_TAG_PARAMETERS.peCount = 0;
                        dsmCookie.peCount = 0;
                    }
                    commonApiParam = survey.api.helper.buildCommonApiParameters(surveyData.JS_TAG_PARAMETERS);
                    surveyData.JS_TAG_PARAMETERS.respondentID = dsmCookie.respondentID;
                    eventSpecificAPIParameters = survey.api.helper.buildEventSpecificApiParameters('invite', surveyData.JS_TAG_PARAMETERS);
                    projectExposureCount = dsmCookie.peCount;

                    if (projectExposureCount === -1 || projectExposureCount === undefined) {
                        eventSpecificAPIParameters.projectExposureCount = surveyData.JS_TAG_PARAMETERS.type !== 'InBanner' ? 0 : '';

                    } else {
                        eventSpecificAPIParameters.projectExposureCount = projectExposureCount + 1;
                        eventSpecificAPIParameters.projectExposureCount = surveyData.JS_TAG_PARAMETERS.type !== 'InBanner' ? projectExposureCount + 1 : projectExposureCount;
                    }


                    mergedAPIParameters = $.extend({}, commonApiParam, eventSpecificAPIParameters);

                    url = survey.environment.getEnvironmentPath().API + '?' + $.param(mergedAPIParameters);
                    survey.dom.create.writeScriptTagToHead(url, 'dsm_script_tag');
                    survey.data.setPersistentSurveyData('DSM_SURVEY_DATA', surveyData);

                }
                window.KNDSM_Response = function (event, data) {
                    if (event === 'SurveyTimeOut') {
                        survey.data.setPersistentSurveyData('RESPONSE_SET_ID', data.responseSetID);
                    }
                };

                window.dimestoreCallback = function (methodCalled, payload) {
                    var surveyTemplates,
                        i,
                        cookieObj,
                        exposure,
                        tagId,
                        tempSurveyData,
                        cookiePath,
                        completedProjects,
                        hasTakenSurvey;
                    tagId = survey.data.getPersistentSurveyData('DSM_SURVEY_DATA').JS_TAG_PARAMETERS.tagId;

                    tempSurveyData = survey.data.getPersistentSurveyData('DSM_SURVEY_DATA');
                    tempSurveyData.JS_TAG_PARAMETERS.exposure = window['data_' + tagId].exposure;
                    survey.data.setPersistentSurveyData('DSM_SURVEY_DATA', tempSurveyData);

                    if (survey.utility.bool.surveyIsLive() === false) {
                        return false;
                    }

                    if (methodCalled === 'requested') {
                        tagType = survey.tags.helper.getTagType();
                        cookieObj = survey.utility.obj.getProjectExposureCount(payload, false);
                        survey.data.setPersistentSurveyData('DSM_COOKIES', cookieObj);

                        if (survey.utility.bool.isSurveyOPTOUT() === true) {
                            tempSurveyData.JS_TAG_PARAMETERS.invitationType = 'OPT-OUT';
                            surveyNotEligible(tempSurveyData);
                        } else {
                            completedProjects = survey.data.getPersistentSurveyData('DSM_COOKIES').dmisrvy_project_complete;
                            hasTakenSurvey = survey.utility.bool.hasCompletedSurvey(tempSurveyData.JS_TAG_PARAMETERS.projectID, completedProjects.split(','));

                            if (survey.utility.bool.oneSurveyPerUser() === true && hasTakenSurvey === true) {
                                tempSurveyData.JS_TAG_PARAMETERS.invitationType = '';
                                surveyNotEligible(tempSurveyData);
                            } else {
                                if (tempSurveyData.JS_TAG_PARAMETERS.globalInvitations && !survey.decisioning.checkSurveyEligibility(payload)) {
                                    surveyNotEligible(survey.data.getPersistentSurveyData('DSM_SURVEY_DATA'));
                                    return;
                                }
                            }
                        }

                        tempSurveyData = survey.data.getPersistentSurveyData('DSM_SURVEY_DATA');
                        tempSurveyData.JS_TAG_PARAMETERS.creativeGroupName = window['data_' + tagId].creativeGroupName || false;
                        survey.data.setPersistentSurveyData('DSM_SURVEY_DATA', tempSurveyData);
                        exposure = tempSurveyData.JS_TAG_PARAMETERS.exposure;

                        if (tempSurveyData.JS_TAG_PARAMETERS.type !== 'InBanner') {
                            if (cookieObj.peCount === -1 || isNaN(cookieObj.peCount) || cookieObj.peCount === 0) {
                                if (exposure !== undefined || exposure !== "" || exposure !== null) {
                                    cookieObj.peCount = 0;
                                } else {
                                    cookieObj.peCount = 1;
                                }
                            }
                            if ("control" === exposure) {
                                cookieObj.inactivityTimeout = "false";
                            } else if ("exposed" === exposure || (exposure === undefined || exposure === "" || exposure === null)) {
                                cookieObj.peCount = parseInt(cookieObj.peCount + 1, 10);
                            }
                        }

                        survey.data.setPersistentSurveyData('DSM_COOKIES', cookieObj);
                    }

                    if (!surveyData) {
                        surveyData = survey.data.getPersistentSurveyData('DSM_SURVEY_DATA');

                        //Get the tag type
                        tagType = survey.tags.helper.getTagType();

                        surveyScreenOrder =
                            survey.tags.helper.getSurveyViewTemplateNamesInOrder(surveyData.SURVEY);

                        survey.data.setPersistentSurveyData('DSM_SURVEY_SCREEN_ORDER', surveyScreenOrder);
                        if (survey.utility.bool.surveyWillLoop()) {
                            survey.data.setPersistentSurveyData('DSM_SURVEY_SCREEN_ORDER_TEMPLATE', surveyScreenOrder);
                        }

                        //Prepend the survey wrapper name
                        surveyScreenOrder.unshift(tagType.wrapperName);
                    }
                    if (surveyData.JS_TAG_PARAMETERS.invitationType === undefined) {
                        cookiePath = survey.cookie.setCookies.buildCookieParam('requested');
                        survey.dom.create.writeScriptTagToHead(cookiePath, 'dsm_script_tag');
                    }
                    if (methodCalled === 'fetchTemplateFiles') {
                        loadedSurveyScreens.push(payload.markup);
                        if (loadedSurveyScreens.length === surveyScreenOrder.length) {
                            allScreensLoaded(loadedSurveyScreens);
                        }
                        return;
                    }
                    surveyTemplates = survey.views.getTemplateFileNames(surveyScreenOrder);

                    //Build an array of real HTML markup
                    for (i = 0; i < surveyTemplates.length; i++) {
                        getTemplateMarkup(surveyTemplates[i]);
                    }
                    return;
                };
            }
        );
    }

    isRequireJSLoaded = setInterval(function () {
        if (!window.require) {
            return;
        }
        clearInterval(isRequireJSLoaded);
        surveyInit();
    }, 100);
}());