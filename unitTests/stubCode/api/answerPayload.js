define({
    payload: {
        "event": "answer",
        "action": "response",
        "questionID": "1022327",
        "questionType": "multi",
        "questionCategory": "None",
        "answerID": "1116339,1116340,1116337",
        "mutuallyExclusive": "false,false,false",
        "playerVersion": "4",
        "version": "14",
        "browserInfo": "Chrome_39.0.2171.95_Desktop_7",
        "responseSetID": "b018140ee5c34a8085a2dc122210d8b7",
        "respondentID": "162bfc48c8304487b267be1a6e7c8acf",
        "accountID": "1",
        "exposure": "exposed",
        "questionnaireID": "105132",
        "projectID": "12078",
        "siteID": "SiteID",
        "identifier": "Ident",
        "adTypeID": "ADD",
        "tagID": "43",
        "projectExposureCount": "2"
    }
});