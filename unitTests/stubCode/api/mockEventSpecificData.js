define({
    requested: {
        "event":"requested",
        "action":"projectRequest",
        "isLive":"false",
        "format": "json",
        "function":"dimestoreCallback"
    },
    projectStart: {
        "event":"projectStart",
        "action":"projectStart",
        "respondentID": "bc7c2de7012241e38e1260ddb9249dfe",
        "surveyShown":"true",
        "invitationValue":12345
    },
    registration: {
        "event":"registration",
        "action":"respondentRegistration",
        "respondentEmail":"test@foo.com",
        "questionnaireID":"105132",
        "projectExposureCount":"11",
        "responseSetID":"82f6ac8b27874b21b5c1c150f6d0e787",
        "respondentID": "bc7c2de7012241e38e1260ddb9249dfe"
    },
    surveyTimeout: {
        "event":"surveyTimeout",
        "action":"questionnaire",
        "questionnaireID":"105132",
        "projectExposureCount":"11",
        "responseSetID":"82f6ac8b27874b21b5c1c150f6d0e787",
        "respondentID": "bc7c2de7012241e38e1260ddb9249dfe"
    },
    answer: {
        "event":"answer",
        "action":"response",
        "questionnaireID":"105132",
        "projectExposureCount":"11",
        "responseSetID":"82f6ac8b27874b21b5c1c150f6d0e787",
        "respondentID": "bc7c2de7012241e38e1260ddb9249dfe",
        "questionID":"1022322",
        "questionType":"one",
        "questionCategory":"None",
        "answerID":"1116321",
        "oeAnswerText": "Hi Mom",
        "mutuallyExclusive":false,
        "pixelExposureCount": 2
    }
});