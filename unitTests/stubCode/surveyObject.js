define({
    "version_major": "1",
    "version_minor": "0",
    "pixels": [],
    "project_info": {
        "width": "300",
        "height": "250",
        "account_id": "1",
        "reg_before_quiz": "true",
        "sound_status": "0",
        "is_live": "true",
        "theme_data_path": "http://content-dimestore.dmi.sensic.net/prod/survey_data/survey_theme/10155/10155.json",
        "version": "5",
        "background_alpha": "100"
    },
    "survey_info": {
        "id": "105190",
        "disable": "0",
        "allow_repeat": "true",
        "inactivity_timeout": "90",
        "loop_on_survey_timeout": "true",
        "inactivity_timeout_show_counter": "false",
        "quiz_meter_visible": "true",
        "number_of_columns": "1",
        "reverse_multi_link_behavior": "false"
    },
    "registration": {
        "enabled": "true",
        "background_color": "000000",
        "message": "Enter your e-mail to register.",
        "text_color": "FFFFFF",
        "textbox_message": "e-mail",
        "validation_method": "email",
        "submit_button_text": "Register",
        "hide_skip_button": "false"
    },
    "multi_size": "false",
    "creatives": [{
        "id": "62192",
        "type": "SWF",
        "url": "http://content-dimestore.dmi.sensic.net/uploaded_files/prod/account1/project12064/banner_blue_300x250.swf",
        "click_url": "http://www.dimestore.com/defaultclick",
        "video_buffer_time": "3",
        "duration": "3",
        "group_name": "default",
        "active_on_mouseover_creative": "false",
        "remove_creative_click_listener": "false",
        "as_version": "3",
        "fp_version": "9"
    }],
    "endscreens": {
        "backup_for_all_groups": {
            "id": "120193",
            "type": "SWF",
            "url": "http://content-dimestore.dmi.sensic.net/uploaded_files/prod/account1/project12064/endscreengroup/banner_ender_blue_300x250.swf",
            "click_url": "http://dimestore.com/defaultclick",
            "as_version": "3",
            "creative_width": "300",
            "creative_height": "250",
            "fp_version": "9"
        },
        "manage_by_group": "false"
    },
    "questions": [{
        "id": "1022230",
        "type": "multi",
        "questionCategory": "None",
        "text": "Which of the following brands of [product category] have you seen advertised online in the past 7 days? (Select all that apply.)",
        "randomize_answers": "true",
        "answers": [{
            "id": "1117783",
            "text": "Study brand",
            "correct": "false",
            "url": "",
            "anchor": "",
            "mutually_exclusive": "false",
            "goto_endscreen": "false"
        }, {
            "id": "1117784",
            "text": "Competitor 1",
            "correct": "false",
            "url": "",
            "anchor": "",
            "mutually_exclusive": "false",
            "goto_endscreen": "false"
        }, {
            "id": "1117785",
            "text": "Competitor 2",
            "correct": "false",
            "url": "",
            "anchor": "",
            "mutually_exclusive": "false",
            "goto_endscreen": "false"
        }, {
            "id": "1117786",
            "text": "Competitor 3",
            "correct": "false",
            "url": "",
            "anchor": "",
            "mutually_exclusive": "false",
            "goto_endscreen": "false"
        }, {
            "id": "1117787",
            "text": "Competitor 4",
            "correct": "false",
            "url": "",
            "anchor": "",
            "mutually_exclusive": "false",
            "goto_endscreen": "false"
        }, {
            "id": "1117788",
            "text": "None of the above",
            "correct": "false",
            "url": "",
            "anchor": "true",
            "mutually_exclusive": "true",
            "goto_endscreen": "false"
        }]
    }, {
        "id": "1022231",
        "type": "one",
        "questionCategory": "None",
        "text": "What is your age and gender?",
        "randomize_answers": "false",
        "answers": [{
            "id": "1117789",
            "text": "Under 18",
            "correct": "false",
            "url": "",
            "anchor": "",
            "mutually_exclusive": "false",
            "goto_endscreen": "false"
        }, {
            "id": "1117790",
            "text": "Male 18-34",
            "correct": "false",
            "url": "",
            "anchor": "",
            "mutually_exclusive": "false",
            "goto_endscreen": "false"
        }, {
            "id": "1117791",
            "text": "Male 35-54",
            "correct": "false",
            "url": "",
            "anchor": "",
            "mutually_exclusive": "false",
            "goto_endscreen": "false"
        }, {
            "id": "1117792",
            "text": "Female 18-34",
            "correct": "false",
            "url": "",
            "anchor": "",
            "mutually_exclusive": "false",
            "goto_endscreen": "false"
        }, {
            "id": "1117793",
            "text": "Female 35-54",
            "correct": "false",
            "url": "",
            "anchor": "",
            "mutually_exclusive": "false",
            "goto_endscreen": "false"
        }, {
            "id": "1117794",
            "text": "55 and over",
            "correct": "false",
            "url": "",
            "anchor": "",
            "mutually_exclusive": "false",
            "goto_endscreen": "false"
        }]
    }]
});