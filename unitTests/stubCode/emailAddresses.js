define({
    emails: [
        'test@dimestore.com',
        'test@dimestore.org',
        'test@dimestore.net',
        'test@dimestore.int',
        'test@dimestore.edu',
        'test@dimestore.gov',
        'test@dimestore.mil'                    
    ]
});