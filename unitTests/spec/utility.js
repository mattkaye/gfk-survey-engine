define(function(require) {
    'use strict';
    var Utility = require('../../modules/utility/Utility');

    describe('///UTILITY MODULE///', function() {
        var module = Utility;

        describe('stripMacros(string)', function() {
            it('Removes the Dimestore macro start and end identifier from a string.', function() {
                var stringWithMacro,
                    cleanString;

                stringWithMacro = 'DMIMACROSHello WorldDMIMACROE';
                cleanString = module.string.stripMacros(stringWithMacro);
                expect(cleanString).toBe('Hello World');

                stringWithMacro = '';
                cleanString = module.string.stripMacros(stringWithMacro);
                expect(cleanString).toBe('');
            });
        });

        describe('getUserAgentInformation()', function() {
            it('Returns an object containing user agent and device information data.', function() {
                var uaObject = module.string.getUserAgentInformation();

                expect(uaObject).toBeDefined();
                expect(uaObject.browser).toBeDefined();
                expect(uaObject.device).toBeDefined();
                expect(uaObject.os).toBeDefined();
                expect(uaObject.ua).toBeDefined();
            });
        });

        describe('formatUserAgentInformationForAPI()', function() {
            it('Returns a user agent and device string in the format that the API expects.', function() {
                var uaObject,
                    uaFormattedString;

                uaObject = module.string.getUserAgentInformation();
                uaFormattedString = uaObject.browser.name + '_' + uaObject.browser.version + '_' + (uaObject.device.model || 'Desktop') + '_' + uaObject.os.version;

                expect(uaFormattedString).toBeNonEmptyString();
            });
        });

        describe('removeParameterFromURLString(key, source)', function() {
            it('Removes a given URL parameter from a URL string and returns the new string.', function() {
                var source,
                    key,
                    resultString;

                source = 'http://api-dimestore.dmi.sensic.net/vi-api/?playerVersion=4&format=jsonp&projectID=12020&siteID=&adTypeID=&tacticID=&userID=&identifier=&exposure=exposed&tagID=14&scriptID=dsmTag14&clickUrl=&action=projectRequest&browserInfo=Chrome_39.0.2171.95_Desktop_7&event=requested&function=captureDMITagData&requestID=zPi3rxRXHwTze0yucUhCy8YnvAhm1LfA&version=4&accountID=1';
                key = 'browserInfo';
                resultString = module.string.removeParameterFromURLString(key, source);
                expect(resultString).not.toMatch(key + '=');
            });
        });

        describe('generateRandomString(length)', function() {
            it('Returns a random set of alphanumeric characters of a varible length.', function() {
                var characterLength,
                    randomString;

                characterLength = 32;
                randomString = module.string.generateRandomString(characterLength);
                expect(randomString.length).toEqual(characterLength);
            });
        });

        describe('getParameterValueByName(parameter_name, source)', function() {
            it('Returns the value of a given URL parameter from the source.', function() {
                var source,
                    result;

                source = 'http://api-dimestore.dmi.sensic.net/vi-api/?playerVersion=4&format=jsonp&projectID=12020&siteID=&adTypeID=&tacticID=&userID=&identifier=&exposure=exposed&tagID=14&scriptID=dsmTag14&clickUrl=&action=projectRequest&browserInfo=Chrome_39.0.2171.95_Desktop_7&event=requested&function=captureDMITagData&requestID=zPi3rxRXHwTze0yucUhCy8YnvAhm1LfA&version=4&accountID=1';

                result = module.string.getParameterValueByName('exposure', source);
                expect(result).toEqual('exposed');
            });
        });

        describe('joinDuplicateURLParameterValues(urlString, separator)', function() {
            it('Given a URL string, this method joins duplicate parameter values under a single parameter with an optional custom separator defined.', function() {
                var urlString,
                    result;

                urlString = 'event=answer&action=response&questionID=1022132&questionType=multi&questionCategory=None&answerID=value_1&answerID=value_2&answerID=value_3&CustomParam=&FooBar=123&FooBar=4567';

                result = module.string.joinDuplicateURLParameterValues(urlString, '|');

                expect(result).toContain('&answerID=value_1|value_2|value_3');
                expect(result).toContain('&FooBar=123|4567');
            });
        });

        describe('isValidEmailAddress(emailAddress)', function() {
            var emailAddresses,
                testAddresses,
                i;

            beforeEach(function(done) {
                function requireCallback(emailAddresses) {
                    testAddresses = emailAddresses;
                    done();
                }

                require(['stubCode/emailAddresses'], function(obj) {
                    emailAddresses = obj.emails;
                    requireCallback(emailAddresses);
                });
            });

            it('Returns true for a well formatted email address.', function() {
                for (i = 0; i < testAddresses.length; i++) {
                    expect(module.bool.isValidEmailAddress(testAddresses[i])).toBe(true);
                }
            });
        });

        describe('hasCompletedSurvey(projectID, completedProjects)', function() {
            it('Retuns a Boolean.', function() {
                var result,
                    projectID,
                    completedProjects;

                result = false;

                projectID = 105098;
                completedProjects = [555, 105098, 3333333];

                result = module.bool.hasCompletedSurvey(projectID, completedProjects);
                expect(result).toBeBoolean();
                expect(result).toBe(true);
            });
        });

        describe('getIntegrationBaseURL(questions)', function() {
            var externalTrackingURL;

            beforeEach(function(done) {
                require(['stubCode/questionsArray'], function(mockObject) {
                    function requireCallback(result) {
                        externalTrackingURL = result;
                        done();
                    }

                    var questions,
                        result;

                    questions = mockObject.questions;

                    result = module.string.getIntegrationBaseURL(questions);
                    requireCallback(result);
                });
            });

            it('Returns the integration url if there is one. False if there is none.', function() {
                expect(externalTrackingURL).toBe('http://www.integrationbase.com');
            });
        });

        describe('shuffleQuestionAnswers(array)', function() {
            var defaultAnswersArray,
                randomizedAnswersArray;

            beforeEach(function(done) {
                require(['stubCode/questionsArray'], function(mockObject) {
                    defaultAnswersArray = $.extend([], mockObject.questions[0].answers);
                    randomizedAnswersArray = module.array.shuffleQuestionAnswers(mockObject.questions[0].answers);
                    done();
                });
            });

            it('Returns an array in a new random order with the exception of those marked as anchors.', function() {
                expect(randomizedAnswersArray).toBeArray();
                expect(randomizedAnswersArray).toBeArrayOfSize(5);
                expect(randomizedAnswersArray).not.toEqual(defaultAnswersArray);
            });
        });

        describe('urlStringToObject(urlString)', function() {
            it('Returns an object representation of a well formatted URL string of paramters.', function() {
                var urlString,
                    result,
                    expectedObject;

                urlString = 'playerVersion=4&format=jsonp&projectID=12020&siteID=google.com';

                result = module.obj.urlStringToObject(urlString);
                expectedObject = {
                    "playerVersion": "4",
                    "format": "jsonp",
                    "projectID": "12020",
                    "siteID": "google.com"
                };
                expect(result).toEqual(expectedObject);
            });
        });

        describe('surveyTimer(options)', function() {
            var timer = module.math.surveyTimer({
                seconds: 65,
                onUpdateStatus: function(remainingTime) {
                    console.log('surveyTimer() ' + remainingTime);
                },
                onCounterEnd: function() {
                    console.log('surveyTimer(): Done!');
                }
            });

            it('Counts down from a set number of seconds to zero. Callbacks available on each second and at the end of the timer.(OPEN THE CONSOLE TO SEE THE COUNTDOWN)', function() {
                timer.start();
                //Unsure how to test this
                expect(true).toBe(true);
            });
        });
        describe('decodeString(string)', function() {
            it('Decoding string if string consist some encoded special character character.', function() {
                var string,
                    resultString;
                string = '%25%24%26';
                resultString = module.string.decodeString(string);
                expect(resultString).toEqual('%$&');
            });
        });
        describe('encodeString(string)', function() {
            it('Encoding string if string consist special character.', function() {
                var string,
                    resultString;
                string = '%$&';
                resultString = module.string.encodeString(string);
                expect(resultString).toEqual('%25%24%26');
            });
        });
        describe('urlOnAnswer(answerIDs)', function() {
            it('Determine if any answer option have link URL.', function() {
                var result,
                    answerId = "1116322";
                result = module.bool.urlOnAnswer(answerId);
                expect(result).toBeBoolean();
            });
        });
        describe('checkUrlAndTerminate(questionData)', function () {
            it('Encoding string if string consist special character.', function() {
                var result,
                    questionData = {
                        answerID: '1116336',
                        questionCategory: 'none',
                        questionID: '1022327',
                        questionType: 'multi'
                    };
                result = module.obj.checkUrlAndTerminate(questionData);
                expect(result).toBeObject();
                expect(result.urlWithTerminate).toBeBoolean();
                expect(result.urlWithNoTerminate).toBeBoolean();
            });
        });
        describe('checkSurveyMaxInvite()', function () {
            it('Check project invite is available or not for Survey.....', function () {
                var result;
                result = module.bool.checkSurveyMaxInvite();
                expect(result).toBeBoolean();
                
            });
        });
    });
});