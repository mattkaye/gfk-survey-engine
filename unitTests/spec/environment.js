define(function (require) {
    'use strict';
    var Environment = require('../../modules/environment/Environment');

    describe('///ENVIRONMENT MODULE///', function () {
        var module = new Environment();

        describe('getEnvironmentPath()', function () {
            var thisEnvironmentObject;
            thisEnvironmentObject = module.getEnvironmentPath();

            it('Returns an object holding paths to the required server end points.', function () {
                expect(thisEnvironmentObject).not.toBeFalsy();
                expect(thisEnvironmentObject.CDN).toBeDefined();
                expect(thisEnvironmentObject.API).toBeDefined();
                expect(thisEnvironmentObject.COOKIES).toBeDefined();
            });
        });
        describe('isSecuredPath(scriptName)', function () {
            var source,
                result;

            source = 'http://stagingadmin2-dimestore.dmi.sensic.net/prod/html5/dimestoreTag.js?projectID=12519&playerVersion=5&identifier=&siteId=&adTypeId=&tacticId=&userId=&exposure=123&scriptID=dsmTag4937&clickUrl="';
            result = module.isSecuredPath(source);
            it('Returns the boolean value of a given URL string whether it is from secured domain.', function () {
               expect(result).toEqual(false);
            });
        });
    });
});