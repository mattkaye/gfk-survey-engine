define(function (require) {
    'use strict';
    var DecisioningModule = require('../../modules/decisioning/Decisioning');
    describe('///DECISIONING MODULE///', function () {
        var module = new DecisioningModule();

        describe('checkSurveyEligibility(config)', function () {
            var result;
            require(['stubCode/api/mockCommonData'], function (config) {
                result = module.checkSurveyEligibility(config);
            });
            it('Check for survey eligibility for invite. Whether survey is invited or not.....', function () {
                expect(result).toBeBoolean();
            });
        });
        
        describe('checkGlobalInvite(config)', function () {
            var result;
            require(['stubCode/api/mockCommonData'], function (config) {
                result = module.checkGlobalInvite(config);
            });
            it('Check Global invite is available or not for Survey.....', function () {
                expect(result).toBeObject();
                expect(result.globalInvite).toBeDefined();

            });
        });
        
        describe('checkProjectInvite(config)', function () {
            var result;
            require(['stubCode/api/mockCommonData'], function (config) {
                result = module.checkProjectInvite(config);
            });
            it('Check project invite is available or not for Survey.....', function () {
                expect(result).toBeObject();
                expect(result.projectInvite).toBeDefined();

            });
        });
        
        describe('checkPercentageInvite(config, studyType)', function () {
            var result;
            require(['stubCode/api/mockCommonData'], function (config) {
                result = module.checkPercentageInvite(config, "CE");
            });
            it('Check percent invite for Survey.....', function () {
                expect(result).toBeObject();
                expect(result.percentInvite.invite).toBeBoolean();

            });
        });
        
        describe('randomlyAssignControlExpose(config)', function () {
            var result;
            require(['stubCode/api/mockCommonData'], function (config) {
                result = module.randomlyAssignControlExpose(config);
            });
            it('Randomly assign Control/Expose.....', function () {
                expect(result).toBeString();
            });
        });
        
        describe('doInviteRandomization(invite, config)', function () {
            var result,
                invite = 0;
            require(['stubCode/api/mockCommonData'], function (config) {
                result = module.doInviteRandomization(invite, config);
            });
            it('Invite randomization.....', function () {
                expect(result).toBeBoolean();
            });
        });
        
        describe('checkPixelExposure(config)', function () {
            var result;
            require(['stubCode/api/mockCommonData'], function (config) {
                result = module.checkPixelExposure(config);
            });
            it('Check for pixel exposure.....', function () {
                expect(result).toBeString();
            });
        });
    });
});