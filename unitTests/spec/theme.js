define(function (require) {
    'use strict';
    var Theme = require('../../modules/theme/Theme'),
        JasminejQuery = require('../lib/jasmine-2.1.3/jasmine-jquery');

    describe('///THEME MODULE///', function () {
        jasmine.getFixtures().fixturesPath = 'spec/../../templates/';

        describe("Fixture Loading", function () {
            var node;
            loadFixtures('testFixture.html');

            node = $('#my-fixture');

            it("Demo of how to load fixtures", function () {
                expect(node).toExist();
            });
        });
    });
});