define(function (require) {
    'use strict';
    var DomModule = require('../../modules/dom/Dom');
        
    describe('///DOM MODULE///', function () {
        var module;

        module = DomModule;
        jasmine.getFixtures().fixturesPath = 'spec/../../templates/';
        
        describe("getTagParameters(scriptTagNode)", function () {
            var node,
                testTag;
            loadFixtures('scriptTagInDOM.html');
            
            testTag = module.read.findScriptTagByFileName('modernizr.min');
            node = module.read.getTagParameters(testTag);

            it("Returns a string of URL parameters to the right of the ? sign or false.", function () {
                expect(node).toBeDefined();
                expect(node).toContain('test=1234&param=true');
            });
        });
        
        describe("findScriptTagByFileName(fileName)", function () {
            var node;
            loadFixtures('scriptTagInDOM.html');

            node = module.read.findScriptTagByFileName('modernizr.min');

            it("Returns the DOM node of the script tag or false.", function () {
                expect(node).not.toBe(false);
            });
        });

        describe("writeScriptTagToHead(scriptPath, tagID)", function () {
            var scriptPath,
                tagID,
                node;

            scriptPath = "http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js";
            tagID = "script-loading-test";

            module.create.writeScriptTagToHead(scriptPath, tagID);

            node = $('#script-loading-test');

            it("Writes a script tag to the head of the document with an optional ID attribute", function () {
                expect(node).toExist();
            });
        });

        describe('writePixelsToDOM(pixelsArray)', function () {
            it('Write any number 1x1 image pixels to the DOM to set cookies.', function () {
                var pixelsArray;

                pixelsArray = ['jasmine', 'jasmine_2', 'jasmine_3'];

                module.create.writePixelsToDOM(pixelsArray);

                expect($('[data-js="dimestore_pixel"]').length).toBe(3);
            });
        });

        describe('canAccessParentDOM()', function () {
            it('Returns true if the script within this scope can access the parent window.', function () {
                expect(module.read.canAccessParentDOM()).toMatch(/true|false/);
            });
        });

        describe('siteIsSecure()', function () {
            it('Returns true if parent window is secure.', function () {
                expect(module.read.siteIsSecure()).toBeBoolean();
            });
        });
        
        describe('cookiesAreEnabled()', function () {
            var result;

            result = module.read.cookiesAreEnabled();

            it('Returns a boolean value based on cookies being enabled. We expect they are enabled for this test', function () {
                expect(result).toBe(true);
            });
        });
        
    });
});