define(function (require) {
    'use strict';
    var ApiModule = require('../../modules/api/Api'),
        UtilityModule = require('../../modules/utility/Utility');

    describe('///API MODULE///', function () {
        var module,
            utility;

        module = new ApiModule();
        utility = UtilityModule;

        describe('postToAPI(payload, callback)', function () {
            var apiResponse,
                callbackTest;

            callbackTest = {
                userDefinedCallback: function () {
                    //This stub function validates that postToAPI() callback was fired.
                }
            };

            beforeEach(function (done) {
                spyOn(callbackTest, 'userDefinedCallback');
                require(['stubCode/api/answerPayload'], function (result) {
                    var payload,
                        callback;
                        
                    payload = result.payload;

                    callback = function (data) {
                        apiResponse = data;
                        callbackTest.userDefinedCallback(apiResponse);
                        done();
                    };
                    apiResponse = module.post.postToAPI(payload, callback);
                });
            });

            it('Posts a payload of data to the API endpoint, with an optional callback function after the post is complete.', function () {
                expect(apiResponse).toBeObject();
                expect(apiResponse.responseSetID).toBeDefined();
                expect(callbackTest.userDefinedCallback).toHaveBeenCalled();
            });
        });

        describe('matchAPIactionToEvent(event)', function () {
            it('Given an API event name, this will return the event and action as an object.', function () {
                var event,
                    eventActionObject;

                event = 'answer';
                eventActionObject = module.helper.matchAPIactionToEvent(event);

                expect(eventActionObject).toBeObject();
                expect(eventActionObject.event).toBeDefined();
                expect(eventActionObject.action).toBeDefined();
            });
        });

        describe('buildCommonApiParameters(parameterData)', function () {
            
            var mockDataForAPI;

            beforeEach(function (done) {
                require(['stubCode/api/mockCommonData'], function (mockObject) {
                    mockDataForAPI = mockObject;
                    done();
                });
            });

            it('Builds the common parameters passed to the API on every request, regardless of the event.', function () {
                var commonApiParameters,
                    expectedProperties,
                    i;
                
                commonApiParameters = module.helper.buildCommonApiParameters(mockDataForAPI);
                
                expectedProperties = [
                    'requestID',
                    'browserInfo',
                    'playerVersion',
                    'accountID',
                    'projectID',
                    'tagID',
                    'version',
                    'tacticID',
                    'adTypeID',
                    'exposure',
                    'userID',
                    'siteID',
                    'identifier',
                    'pixelIDs',
                    'size'
                ];
                
                for (i = 0; i < expectedProperties.length; i++) {
                    expect(commonApiParameters[expectedProperties[i]]).toBeDefined();
                    expect(Object.keys(commonApiParameters).length).toEqual(expectedProperties.length);
                }
            });
        });

        describe('buildEventSpecificApiParameters(eventName, parameterData)', function () {
            var mockDataForAPI;

            beforeEach(function (done) {
                require(['stubCode/api/mockEventSpecificData'], function (mockObject) {
                    mockDataForAPI = mockObject;
                    done();
                });
            });
            
            it('Builds parameters passed to the API only on a specific event request.', function () {
                var eventSpecificApiParameters,
                    expectedProperties,
                    events,
                    eventName,
                    propertyName,
                    key,
                    i;
                
                eventSpecificApiParameters = {};
                expectedProperties = {};
                
                events = [
                    'requested',
                    'projectStart',
                    'registration',
                    'answer',
                    'surveyTimeout'
                ];
                
                for (i = 0; i < events.length; i++) {
                    eventSpecificApiParameters[events[i]] = module.helper.buildEventSpecificApiParameters(events[i], mockDataForAPI[events[i]]);    
                }

                expectedProperties.requested = [
                    'event',
                    'action',
                    'format',
                    'function'
                ];
                
                expectedProperties.projectStart = [
                    'event',
                    'action',
                    'respondentID',
                    'surveyShown',
                    'invitationValue',
                    'creativeGroupName',
                    'creativeID',
                    'projectExposureCount'
                ];
                
                expectedProperties.registration = [
                    'event',
                    'action',
                    'respondentEmail',
                    'questionnaireID',
                    'projectExposureCount',
                    'responseSetID',
                    'respondentID'
                ];
                
                expectedProperties.answer = [
                    'event',
                    'action',
                    'questionnaireID',
                    'projectExposureCount',
                    'responseSetID',
                    'respondentID',
                    'questionID',
                    'questionType',
                    'questionCategory',
                    'answerID',
                    'mutuallyExclusive',
                    'creativeGroupName',
                    'creativeID',
                    'pixelExposureCount'
                ];
                
                expectedProperties.surveyTimeout = [
                    'event',
                    'action',
                    'questionnaireID',
                    'projectExposureCount',
                    'responseSetID',
                    'respondentID',
                    'function'
                ];
                
                for (key in eventSpecificApiParameters) {
                    if (eventSpecificApiParameters.hasOwnProperty(key)) {
                        eventName = eventSpecificApiParameters[key].event;
                        for(i = 0; i < expectedProperties[eventName].length; i++){
                            propertyName = expectedProperties[eventName][i];
                            expect(eventSpecificApiParameters[eventName][propertyName]).toBeDefined();
                            expect(Object.keys(eventSpecificApiParameters[eventName]).length).toEqual(expectedProperties[eventName].length);
                        }
                    }
                }
            });
        });

    });
});