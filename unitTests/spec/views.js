define(function (require) {
    'use strict';
    var ViewsModule = require('../../modules/views/Views');
    describe('///VIEWS MODULE///', function () {
        var module = new ViewsModule();
        
        describe('getTemplateFileNames(templateTypes)', function () {
            var templates;
            
            beforeEach(function (done) {
                require(['stubCode/surveyTemplateTypes'], function (result) {
                    templates = result.data;
                    done();
                });
            });
            
            it('Returns the file path for each of the template types given.', function () {
                var result = module.getTemplateFileNames(templates);
                expect(result.length).toBe(7);
                expect(result).not.toContain(false);
            });
        });
    });
});