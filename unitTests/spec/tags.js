define(function(require) {
    'use strict';
    var TagsModule = require('../../modules/tags/Tags'),
        _ = require('../../bowerComponents/lodash/lodash');

    describe('///TAGS MODULE///', function() {
        var module = new TagsModule();

        describe('getSurveyViewTemplateNamesInOrder(jsonData)', function() {
            var jsonData;

            beforeEach(function(done) {
                require(['stubCode/surveyObject'], function(result) {
                    jsonData = result;
                    done();
                });
            });

            it('Given survey data, it returns an array of what template screens are required in order.', function() {
                var result = module.helper.getSurveyViewTemplateNamesInOrder(jsonData);

                expect(result).toBeArrayOfSize(5);
                expect(result).toBeArrayOfStrings();
                expect(result).toEqual(['creative', 'registration', 'multi', 'one', 'endScreenCustom']);
            });
        });

        describe('getTagType()', function() {
            var jsonData;

            beforeEach(function(done) {
                require(['stubCode/tagTypes'], function(result) {
                    jsonData = result;
                    done();
                });
            });

            it('Look up the target platform and type for a given tag.', function() {
                var thisTagType,
                    result,
                    condition;

                thisTagType = jsonData.mobile[0].flyover;
                result = module.helper.getTagType(thisTagType);
                condition = _.isEqual(result, {
                    targetPlatform: 'mobile web',
                    type: 'flyover',
                    wrapperName: 'wrapperMobileWeb'
                });
                expect(condition).toBeTrue();

                thisTagType = jsonData.desktop[0].inBanner;
                result = module.helper.getTagType(thisTagType);
                condition = _.isEqual(result, {
                    targetPlatform: 'desktop',
                    type: 'in banner',
                    wrapperName: 'wrapperDesktopStandard'
                });
                expect(condition).toBeTrue();

                thisTagType = jsonData.desktop[0].flyover;
                result = module.helper.getTagType(thisTagType);
                condition = _.isEqual(result, {
                    targetPlatform: 'desktop',
                    type: 'flyover',
                    wrapperName: 'wrapperDesktopFlyover'
                });
                expect(condition).toBeTrue();

                thisTagType = jsonData.desktop[0].overlay;
                result = module.helper.getTagType(thisTagType);
                condition = _.isEqual(result, {
                    targetPlatform: 'desktop',
                    type: 'overlay',
                    wrapperName: 'wrapperDesktopStandard'
                });
                expect(condition).toBeTrue();
            });
        });

    });
});