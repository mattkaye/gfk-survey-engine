(function() {
    'use strict';
    // Configure RequireJS to shim Jasmine
    require.config({
        paths: {
            jasmine: 'lib/jasmine-2.1.3/jasmine',
            jasmineHtml: 'lib/jasmine-2.1.3/jasmine-html',
            jasmineExpect: 'lib/jasmine-2.1.3/jasmine-matchers',
            boot: 'lib/jasmine-2.1.3/boot',
            api: '../modules/api/Api',
            utility: '../modules/utility/Utility',
            environment: '../modules/environment/Environment',
            decisioning: '../modules/decisioning/Decisioning',
            data: '../modules/data/Data',
            theme: '../modules/theme/Theme',
            dom: '../modules/dom/Dom',
            views: '../modules/views/Views',
            tags: '../modules/tags/Tags',
            jquery: '../bowerComponents/jquery/dist/jquery',
            cssjson: '../node_modules/cssjson/cssjson',
            mustache: '../bowerComponents/mustache/mustache',
            lodash: '../bowerComponents/lodash/lodash',
            storage: '../bowerComponents/store-js/store',
            uaParser: '../bowerComponents/ua-parser-js/src/ua-parser'
        },
        shim: {
            jasmine: {
                exports: 'window.jasmineRequire'
            },
            'jasmineHtml': {
                deps: [
                    'jasmine'
                ],
                exports: 'window.jasmineRequire'
            },
            boot: {
                deps: [
                    'jasmine',
                    'jasmineHtml'
                ],
                exports: 'window.jasmineRequire'
            }
        }
    });

    // Define all of your specs here. These are RequireJS modules.
    var specs = [
            'spec/utility',
            'spec/api',
            'spec/data',
            'spec/theme',
            'spec/decisioning',
            'spec/environment',
            'spec/dom',
            'spec/views',
            'spec/tags',
            'jasmineExpect'
        ];

    require(['boot'], function() {
        require(specs, function() {
            window.onload();
        });
    });
}());