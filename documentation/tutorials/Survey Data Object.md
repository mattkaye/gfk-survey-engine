Nearly all of the front end logic is directly working off of the JSON data for the survey requested from the API. Since we want to limit our reliance on JavaScript globals, this data is stored using HTML5 local storage. There survey object that is stored contains not only all the data specific to the survey, but the theme data for this particular survey.

Various methods thoughout the framework directly refernce this locally stored object to do their work, so it's important that the object and it's properties are well understood. Here's an example of how this complete object looks:

```javascript
{
    "SURVEY": {
        "version_major": "1",
        "version_minor": "0",
        "pixels": [],
        "project_info": {
            "account_id": "1",
            "reg_before_quiz": "false",
            "is_live": "true",
            "theme_data_path": "http://content-dimestore.dmi.sensic.net/prod/survey_data/survey_theme/13197/13197.json",
            "version": "9",
            "background_alpha": "100"
        },
        "survey_info": {
            "id": "105727",
            "disable": "true",
            "allow_repeat": "true",
            "inactivity_timeout": "90",
            "loop_on_survey_timeout": "true",
            "inactivity_timeout_show_counter": "false",
            "quiz_meter_visible": "true",
            "number_of_columns": "0",
            "reverse_multi_link_behavior": "false"
        },
        "multi_size": "false",
        "registration": {
            "enabled": "true",
            "background_color": "0007D4",
            "message": "Enter your e-mail to register.",
            "text_color": "FFFFFF",
            "textbox_message": "e-mail",
            "validation_method": "email",
            "submit_button_text": "Register",
            "hide_skip_button": "false"
        },
        "questions": [{
            "id": "1024233",
            "type": "one",
            "questionCategory": "None",
            "text": "choose 1",
            "randomize_answers": "false",
            "answers": [{
                "id": "1127968",
                "text": "one",
                "correct": "false",
                "url": "",
                "anchor": "false",
                "mutually_exclusive": "false",
                "goto_endscreen": "false"
            }, {
                "id": "1127969",
                "text": "two",
                "correct": "false",
                "url": "",
                "anchor": "false",
                "mutually_exclusive": "false",
                "goto_endscreen": "false"
            }, {
                "id": "1127970",
                "text": "three",
                "correct": "false",
                "url": "",
                "anchor": "false",
                "mutually_exclusive": "false",
                "goto_endscreen": "false"
            }, {
                "id": "1127971",
                "text": "four",
                "correct": "false",
                "url": "",
                "anchor": "false",
                "mutually_exclusive": "false",
                "goto_endscreen": "false"
            }, {
                "id": "1127972",
                "text": "five",
                "correct": "false",
                "url": "",
                "anchor": "false",
                "mutually_exclusive": "false",
                "goto_endscreen": "false"
            }]
        }, {
            "id": "1024234",
            "type": "multi",
            "questionCategory": "Favorability",
            "text": "Multiple Choice",
            "randomize_answers": "true",
            "answers": [{
                "id": "1127973",
                "text": "one",
                "correct": "false",
                "url": "",
                "anchor": "true",
                "mutually_exclusive": "false",
                "goto_endscreen": "false"
            }, {
                "id": "1127974",
                "text": "two",
                "correct": "false",
                "url": "",
                "anchor": "false",
                "mutually_exclusive": "false",
                "goto_endscreen": "false"
            }, {
                "id": "1127975",
                "text": "three",
                "correct": "false",
                "url": "",
                "anchor": "false",
                "mutually_exclusive": "true",
                "goto_endscreen": "false"
            }, {
                "id": "1127976",
                "text": "four",
                "correct": "false",
                "url": "",
                "anchor": "false",
                "mutually_exclusive": "false",
                "goto_endscreen": "false"
            }, {
                "id": "1127977",
                "text": "five",
                "correct": "false",
                "url": "",
                "anchor": "false",
                "mutually_exclusive": "false",
                "goto_endscreen": "false"
            }]
        }, {
            "id": "1024235",
            "type": "demo",
            "text": "answer a few",
            "required": "true",
            "questions": [{
                "id": "1024236",
                "text": "Do you have children living at home?",
                "questionCategory": "None",
                "answers": [{
                    "id": "16",
                    "label": "Select One",
                    "is_default_answer": "true"
                }, {
                    "id": "17",
                    "label": "No",
                    "is_default_answer": "false"
                }, {
                    "id": "18",
                    "label": "Yes, at least 1 child under 3",
                    "is_default_answer": "false"
                }, {
                    "id": "19",
                    "label": "Yes, at least 1 child under 10",
                    "is_default_answer": "false"
                }, {
                    "id": "20",
                    "label": "Yes, at least 1 under 17",
                    "is_default_answer": "false"
                }, {
                    "id": "21",
                    "label": "Yes, children 18 or older",
                    "is_default_answer": "false"
                }]
            }, {
                "id": "1024237",
                "text": "What is your income?",
                "questionCategory": "None",
                "answers": [{
                    "id": "146",
                    "label": "Select One",
                    "is_default_answer": "true"
                }, {
                    "id": "147",
                    "label": "<50K",
                    "is_default_answer": "false"
                }, {
                    "id": "148",
                    "label": "50-74K",
                    "is_default_answer": "false"
                }, {
                    "id": "149",
                    "label": "75-100K",
                    "is_default_answer": "false"
                }, {
                    "id": "150",
                    "label": "100K+",
                    "is_default_answer": "false"
                }, {
                    "id": "151",
                    "label": "Decline to answer",
                    "is_default_answer": "false"
                }]
            }]
        }, {
            "id": "1024238",
            "type": "open",
            "questionCategory": "None",
            "text": "Give me some text",
            "required": "true",
            "char_limit": "50",
            "input_type": "area"
        }]
    },
    "JS_TAG_PARAMETERS": {
        "type": "InBanner",
        "version": 9,
        "tagType": 1,
        "pid": 12244,
        "accountId": 1,
        "delay": 5,
        "playerVersion": 4,
        "tagId": "dsmTag22",
        "escapeClickUrl": false,
        "inactivityTimeoutValue": 30,
        "inactivityTimeout": false,
        "isLive": false
    },
    "THEME": {
        "project": {
            "background_color1": "",
            "background_color2": "",
            "question_background_color1": "",
            "question_background_color2": "",
            "custom_logo": "http://content-dimestore.dmi.sensic.net/prod/mobile/includes/gfk_logo.svg",
            "invite_overlay": {
                "delay": "",
                "window": "10",
                "button_text": "Take A Quick Survey!",
                "button_color": "e55a00",
                "text_color": "fff"
            }
        },
        "question_container": {
            "text_color": "",
            "font_name": "",
            "font_size": ""
        },
        "answer_container": {
            "text_color": "",
            "font_name": "",
            "font_size": ""
        },
        "continue_button": {
            "background_color": "",
            "text_color": "",
            "text": ""
        },
        "links": [{
            "text_color": "",
            "text": "",
            "font_size": "",
            "url": ""
        }]
    }
}
```