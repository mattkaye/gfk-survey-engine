The mobile web survey can also be run as a stand alone unit on it's own page. This is particularly useful in cases where you want to serve a simple in app banner with a link to the external survey. Assuming we're running on localhost, the path to the stand alone survey is:

http://localhost/frontend-framework/survey.html

In order for the survey to run properly, it's required that a few project parameters that are typically passed in the script tag are now passed in the URL string:

http://localhost/frontend-framework/survey.html?projectID=12244&playerVersion=4&scriptID=dsmTag22&exposure=exposed&inApp=true