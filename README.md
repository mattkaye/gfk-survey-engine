# GFK Survey Engine
GFK is a large multi-national data collection institution that analyzes consumer purchasing habits. One way they do this is by running surveys on thousands of websites worldwide. My code replaces their outdated Adobe Flash based survey engine in favor of HTML5 and JavaScript. The surveys were highly customizable via a back end admin panel, and shipped with a full suite of unit tests. Moving away from Flash meant that surveys would now be seen on mobile devices, which was a huge catalyst for this change in direction.

The system worked by use of script tags added to the client website where the survey was slotted to run:
```
<script type="text/javascript" src="https://mobileapp-dimestore.dmi.sensic.net/gfkTag-v45Xghj.js"></script>
```
This tag would write a JSONP object to the page which my code consumed and built the survey based on its directives. A creative was shown for a pre determined time, followed by questions about that creatives brand. The answers provided were then posted back a Java powered back end where they were put into a queue for Hadoop processing.

The API endpoints and JSONP objects have since been disabled or changed so the screenshots below are what the engine looked like when running.

### Documentation
![](https://png.icons8.com/document/color/30/000000) [Code Overview](http://138.197.95.142/matt-portfolio/gfk-survey-engine/documentation)

### Screenshots
##### Creative shows in masthead:
![alt text](https://i.imgur.com/ujCfTZo.png)

---

##### Creative survey questions about the brand just seen:
![alt text](https://i.imgur.com/J2k3BML.png)

---

![alt text](https://i.imgur.com/AxOZsHm.png)

---

![alt text](https://i.imgur.com/ACFTHVa.png)

### Jasimine Behavior Driven Unit Testing Suite:
[![alt text](https://i.imgur.com/yKy4KtX.png)](https://i.imgur.com/yKy4KtX.png)